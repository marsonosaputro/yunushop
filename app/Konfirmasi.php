<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Konfirmasi extends Model
{
    protected $fillable = ['nama', 'alamat', 'nohp', 'bank', 'rekening', 'atasnama', 'nominal'];
}
