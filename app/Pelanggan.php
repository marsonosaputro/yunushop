<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    protected $fillable = ['user_id', 'namalengkap', 'alamat', 'kota', 'provinsi', 'kodepos', 'keterangan','nohp', 'email'];
}
