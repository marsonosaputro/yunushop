<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\PelanganRequest;
use App\user;
use App\Pelanggan;
use App\Order;
use App\Master;
use App\Produk;
use App\Ongkir;
use Auth;
use Cart;
use DB;
use Mail;


class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function account()
    {
      $data['page_title'] = 'Data User';
      $data['pelanggan'] = Pelanggan::where('user_id', '=', Auth::user()->id)->first();
      return view('orders.account', $data);
    }

    public function editaccount()
    {

      $data['page_title'] = 'Ubah Data User';
      $data['pelanggan'] = Pelanggan::where('user_id', '=', Auth::user()->id)->first();
      $data['kota'] = Ongkir::lists('kota', 'kota');
      return view('orders.editaccount', $data);
    }

    public function order()
    {
        $data['page_title'] = 'Data Order User';
        $data['cart_content'] = Cart::content();
        $data['histori'] = Master::where('user_id', '=', Auth::user()->id)->get();
        return view('orders.order', $data)->with('no', 1);
    }

    public function historiorder($master_kode)
    {
      $data['page_title'] = 'Data Detail Histori Order';
      $data['histori'] = Order::where('master_kode', '=', $master_kode)->get();
      return view('orders.detailhistori', $data)->with('no', 1);
    }

    public function savePelanggan(PelanganRequest $request)
    {
      $data = $request->all();
      $data['user_id'] = Auth::user()->id;
      $cek = Pelanggan::where('user_id', '=', Auth::user()->id)->first();

      if (!empty($cek)) {
        $cek->update($data);
      }else{
        Pelanggan::create($data);
      }

      return redirect('user/cart/account');
    }

    public function saveOrder()
    {
      $kode = time();
      Master::create(['user_id'=>Auth::user()->id, 'kode'=>$kode, 'status'=>'baru', 'totalharga'=>Cart::total()]);

      $cart = Cart::content();
      foreach ($cart as $key => $d)
      {
        $data['master_kode'] = $kode;
        $data['produk_id'] = $d->id;
        $data['jumlah'] = $d->qty;
        $data['harga'] = $d->price;
        $t = Produk::where('id', '=', $data['produk_id'])->first()->stock;
        $jml = $t - $data['jumlah'];
        DB::table('produks')->where('id', '=', $data['produk_id'])->update(['stock'=> $jml]);
        Order::create($data);
      }
      Cart::destroy();
      return redirect('/user/cart/ordersukses');
    }

    public function orderSukses()
    {
      $id = Master::where('user_id', '=', Auth::user()->id)->orderBy('created_at', 'desc')->first();
      $data['total'] = $id->totalharga;
      $data['order'] = Order::where('master_kode', '=', $id->kode)->get();

      $to = Auth::user()->email;

      $subject = 'Konfirmasi Pesanan';

      $headers = "From: butikanis@gmail.com\r\n";
      $headers .= "Reply-To: support@butikanis.com\r\n";
      $headers .= "CC: marsonosaputro@gmail.com\r\n";
      $headers .= "MIME-Version: 1.0\r\n";
      $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
      $message = 'Pesan Konfirmasi';
      mail($to, $subject, $message, $headers);
      //SEND EMAIL
      /**
      Mail::send('orders.email', $data, function ($message) {
          $message->from('butikanis@gmail.com', 'Butik Anis');
          $message->sender('butikanis@gmail.com', 'Butik Anis');

          $message->to(Auth::user()->email, Auth::user()->name);
          $message->replyTo('butikanis@gmail.com', 'Butik Anis');

          $message->subject('Konfirmasi Order dari Butik Anis');
          $message->priority(1);


      });
**/
      //View Sukses
      return view('orders.sukses', $data);
    }

}
