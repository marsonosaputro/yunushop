<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\SimpanStatisRequest;
use Image;
Use App\Statis;
use File;

class StatisController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Daftar Halaman Statis';
        $data['statis'] = Statis::all();
        $data['no'] = 1;
        return view('statis.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('statis.create', ['page_title'=>'Tambah Halaman Statis']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SimpanStatisRequest $request)
    {
        if(!empty($request->file('gambar'))){
            $gambar = time().$request->file('gambar')->getClientOriginalName();
            //$request->file('gambar')->move('gambar/statis', $gambar);
            //$img = Image::make(public_path().'/gambar/statis/'.$gambar)->resize(1000,400);
            //$img->save();
        }else{
            $gambar = '';
        }

        $statis = $request->all();
        $statis['seo'] = str_slug($request['judul'], '-');
        $statis['gambar'] = $gambar;
        Statis::create($statis);
        return redirect('/admin/statis');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Ubah Halaman Statis';
        $data['statis'] = Statis::findOrFail($id);
        return view('statis.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Statis::findOrFail($id);
        if(!empty($request->file('gambar'))){
            $gambar = time().$request->file('gambar')->getClientOriginalName();
            $request->file('gambar')->move('gambar/statis', $gambar);
            //$img = Image::make(public_path().'/gambar/statis/'.$gambar)->resize(1000,400);
            //$img->save();
            File::delete('gambar/statis/'.$data->gambar);
        }else{
            $gambar = $data->gambar;
        }

        $statis = $request->all();
        $statis['seo'] = str_slug($request['judul'], '-');
        $statis['gambar'] = $gambar;
        $data->update($statis);
        return redirect('/admin/statis');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Statis::findOrFail($id);
        File::delete('gambar/statis/'.$data->gambar);
        $data->delete();
        return redirect('admin/statis');
    }
}
