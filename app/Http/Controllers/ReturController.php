<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Retur;
use Session;

class ReturController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $rules = [
      'nama' => 'required|min:7',
      'alamat' => 'required|min:10',
      'nohp' => 'required|numeric',
      'namabarang' => 'required|min:3',
      'alasan' => 'required',
      'pilihan' => 'required'
    ];

    public function __construct()
    {
      $this->middleware('auth', ['except'=>['create','store']]);
    }

    public function index()
    {
        $data['page_title'] = 'Data Pengajuan Retur';
        $data['retur'] = Retur::orderBy('id', 'asc')->paginate(10);
        return view('retur.index', $data)->with('no', $data['retur']->firstItem());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Retur Confirm';
        return view('retur.formretur', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        Retur::create($request->all());
        Session::flash('flash_notification', [
                'level'=>'success',
                'message'=>'Retur berhasil terkirim, tunggu konfirmasi dari kami.'
              ]);
        return redirect(route('confirm.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Retur::findOrFail($id)->delete();
        return redirect('confirm');
    }
}
