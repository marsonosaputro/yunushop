<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Master;
use App\Order;
use App\Pelanggan;
use App\Produk;
use DB;

class PesananController extends Controller
{
    public function index()
    {
    	$data['page_title'] = 'Data Pesanan';
    	$data['pesanan'] = Master::orderBy('id', 'desc')->get();
      $data['no'] = 1;
    	return view('pesanan.daftar', $data);
    }

    public function detailOrder($kode)
    {
      $data['page_title'] = 'Detail Order';
      $data['pesanan'] = Order::where('master_kode', '=', $kode)->get();
      $data['total'] = Master::where('kode', '=', $kode)->first();
      $data['pemesan'] = Pelanggan::where('user_id', '=', $data['total']->user_id)->first();
      $data['no'] = 1;
      $data['option'] = ['baru'=>'baru','tunggu transfer'=>'tunggu transfer', 'transfer'=>'transfer', 'terkirim'=>'terkirim', 'confirm'=>'confirm'];
      return view('pesanan.detail', $data);
    }

    public function ubahStatus(Request $request)
    {
      DB::table('masters')->where('kode', $request->kode)->update(['status' => $request->status]);
      return redirect('admin/pesanan');
    }

    public function hapusPesanan(Request $request)
    {
      $pesanan = Order::where('master_kode', $request->kode)->get();
      foreach ($pesanan as $key => $r) {
        $produk = Produk::where('id', '=', $r->produk_id)->first();
        $up['stock'] = $produk->stock + $r->jumlah;
        $produk->update($up); 
      }
      DB::table('orders')->where('master_kode', '=', $request->kode)->delete();
      DB::table('masters')->where('kode', '=', $request->kode)->delete();
      return redirect('admin/pesanan');
    }
}
