<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class LaporanController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
    	$data['page_title'] = 'Laporan Penjualan';
    	$data['penjualan'] = DB::table('orders')
    								->select(DB::raw('sum(jumlah) as jumlah, produk_id, harga'))
                                    ->groupBy('produk_id')
                                    ->join('masters', function($join){
                                    	$join->on('masters.kode', '=', 'orders.master_kode')
    	                        	     ->where('masters.status', '=', 'terkirim')
    	                        	     ->orWhere('masters.status', '=', 'confirm');
                                    })
                                    ->get();
    	$data['no'] = 1;
    	return view('pesanan.laporan', $data);
    }
}
