<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Produk;
use App\Kategori;
use Cart;
use DB;
use Auth;

class TokoController extends Controller
{
    public function galleryProduk()
    {
        $data['page_title'] = 'Gallery Produk';
        $data['produk'] = Produk::orderBy('id', 'desc')->paginate(6);
        $data['kategori'] = kategori::all();
        return view('frontend.galleryProduk', $data);
    }

    public function detailproduk($seo)
    {
        $data['page_title'] = 'Butik Anis Produk';
        $data['detail'] = Produk::where('seo', '=', $seo)->first();
        $data['kategori'] = kategori::all();
        $data['terbaru'] = Produk::orderBy('id', 'desc')->take(3)->get();
        $data['hits'] = Produk::orderBy('dibaca', 'desc')->take(3)->get();

        $hits = $data['detail']->dibaca + 1;
        DB::table('produks')->where('seo', $seo)->update(['dibaca'=>$hits]);
        return view('frontend.detailproduk', $data);
    }

    public function cart($id)
    {
        $product = Produk::findOrFail($id);

        $data = [
            'id' => $product->id,
            'name' => $product->judul,
            'qty' => 1,
            'price' => $product->harga,
            'options' => ['img'=>$product->gambar, 'berat'=>$product->berat]
        ];
        Cart::add($data);
        return redirect('/cart-content');
    }

    public function cartContent()
    {
        $data['page_title'] = 'Daftar Pesanan';
        $data['cart_content'] = Cart::content();
        $data['no'] = 1;
        $data['id'] = 1;
        $data['terbaru'] = Produk::orderBy('id', 'desc')->take(3)->get();
        return view('frontend.cart', $data);
    }

    public function updateCart(Request $request)
    {
        for ($i=1; $i <= $request->jenis ; $i++) {
          Cart::update($request['id'.$i], $request['qty'.$i]);
        }
        return redirect('/cart-content');
    }

    public function deleteCart($rowid)
    {
        Cart::remove($rowid);
        return redirect('/cart-content');
    }

    public function checkout()
    {
        if (Auth::check()) {
            return redirect('/user/cart/orders');
        }else{
            $data['title'] = 'Proses pembayaran';
            return view('frontend.checkout', $data);
        }

    }

    public function kategoriproduk($kategori_id)
    {
        $data['title'] = 'Data Produk Per Kategori';
        $data['kategori'] = kategori::all();
        $data['produk'] = Produk::where('kategori_id', '=', $kategori_id)->paginate(6);
        return view ('frontend.kategoriproduk', $data);
    }

    public function destroy()
    {
        Cart::destroy();
        return redirect('/');
    }

}
