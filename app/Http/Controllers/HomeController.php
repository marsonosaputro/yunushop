<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Slideshow;
use App\Statis;
use App\Produk;
use App\Artikel;
use App\Kategori;
use App\KategoriArtikel;
use App\Ongkir;
use DB;
use Cart;

class HomeController extends Controller
{
      /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['slideshow'] = Slideshow::orderBy('id', 'desc')->take(4)->get();
        $data['statis1'] = Statis::findOrFail(1);
        $data['statis2'] = Statis::findOrFail(2);
        $data['statis3'] = Statis::findOrFail(3);
        $data['produk'] = Produk::orderBy('id', 'desc')->take(10)->get();
        $data['artikel'] = Artikel::orderBy('id', 'desc')->take(2)->get();
        return view('frontend.home', $data);
    }


    public function artikel($seo)
    {
        $data['artikel'] = Artikel::where('seo', '=', $seo)->first();
        $data['page_title'] = 'Artikel';
        $data['kategori'] = KategoriArtikel::all();
        return view('frontend.artikel', $data);
    }
    public function blog()
    {
        $data['artikel'] = Artikel::orderBy('id', 'desc')->paginate(2);
        $data['page_title'] = 'Blog | Semua Artikel';
        $data['kategori'] = KategoriArtikel::all();
        return view('frontend.blog', $data);
    }

    public function blogKategori($id)
    {
        $data['artikel'] = Artikel::where('kategori_artikel_id', '=', $id)->paginate(2);
        $data['page_title'] = 'Blog | Semua Artikel';
        $data['kategori'] = KategoriArtikel::all();
        return view('frontend.blog', $data);
    }

    public function caraBelanja()
    {
        $data['page_title'] = 'Cara Belanja';
        $data['statis'] = Statis::where('id', '=', 5)->first();
        return view('frontend.caraBelanja', $data);
    }

    public function kontak()
    {
        $data['page_title'] = 'Kontak Kami';
        return view('frontend.kontak', $data);
    }

    public function about()
    {
        $data['page_title'] = 'Profil Butik Anis';
        $data['about'] = Statis::where('id', '=', 4)->first();
        return view('frontend.about', $data);
    }

    public function retur()
    {
        $data['page_title'] = 'Butik Anis';
        $data['about'] = Statis::where('id', '=', 7)->first();
        return view('frontend.retur', $data);
    }

    public function wellcome()
    {
        $data['page_title'] = 'Selamat datang di YunusSHOP';
        $data['about'] = Statis::where('id', '=', 6)->first();
        return view('frontend.about', $data);
    }

    public function biayaongkir()
    {
      $data['page_title'] = 'Biaya Ongkir';
      $data['ongkir'] = Ongkir::orderBy('id', 'desc')->paginate(10);
      return view('frontend.ongkir', $data)->with('no', $data['ongkir']->firstItem());
    }


}
