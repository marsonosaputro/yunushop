<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Ongkir;

class OngkirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Manajement Ongkir';
        $data['ongkir'] = Ongkir::orderBy('id', 'desc')->paginate(10);
        return view('ongkir.index', $data)->with('no', $data['ongkir']->firstItem());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['page_title'] = 'Manajement Ongkir';
      return view('ongkir.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'kota' => 'required',
          'harga' => 'required'
        ]);
        Ongkir::create($request->all());
        return redirect('admin/ongkir');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['page_title'] = 'Manajement Ongkir';
      $data['ongkir'] = Ongkir::findOrFail($id);
      return view('ongkir.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'kota' => 'required',
        'harga' => 'required'
      ]);
      $ongkir = Ongkir::findOrFail($id);
      $ongkir->update($request->all());
      return redirect('admin/ongkir');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Ongkir::findOrFail($id)->delete();
      return redirect('admin/ongkir');
    }
}
