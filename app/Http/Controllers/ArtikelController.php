<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Artikel;
use App\KategoriArtikel;
use Image;
use ImageManager;
use Auth;

class ArtikelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    protected $rules = [
        'judul' => 'required|min:5|unique:artikels',
        'artikel' => 'required|min:5',
        'gambar' => 'required|image',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Artikel';
        $data['artikel'] = Artikel::orderBy('id', 'desc')->paginate(10);
        $data['no'] = $data['artikel']->firstItem();
        return view('artikel.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Tambah Artikel';
        $data['kategori'] = KategoriArtikel::lists('judul', 'id');
        return view('artikel.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        if(!empty($request->file('gambar'))){
            $gambar = time().$request->file('gambar')->getClientOriginalName();
            $request->file('gambar')->move('gambar/artikel/', $gambar);
            $img = Image::make(public_path().'/gambar/artikel/'.$gambar)->crop(1000,400);
            $img->save();
        }else{
            $gambar = '';
        }
        $data = $request->all();
        $data['seo'] = str_slug($request['judul'], '-');
        $data['penulis'] = Auth::user()->name;
        $data['gambar'] = $gambar;
        Artikel::create($data);
        return redirect('/admin/artikel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Ubah Artikel';
        $data['artikel'] = Artikel::findOrFail($id);
        $data['kategori'] = KategoriArtikel::lists('judul', 'id');
        return view('artikel.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['judul'=>'required|min:5', 'artikel'=>'required|min:10']);
        $artikel = Artikel::findOrFail($id);
        if(!empty($request->file('gambar'))){
            $gambar = time().$request->file('gambar')->getClientOriginalName();
            $request->file('gambar')->move('gambar/artikel/', $gambar);
            $img = Image::make(public_path().'/gambar/artikel/'.$gambar)->crop(1000,400);
            $img->save();
            ImageManager::deleteImage( public_path() . '/gambar/artikel/' . $artikel->gambar);
        }else{
            $gambar = $artikel->gambar;
        }

        $data = $request->all();
        $data['seo'] = str_slug($request['judul'], '-');
        $data['penulis'] = Auth::user()->name;
        $data['gambar'] = $gambar;

        $artikel->update($data);
        return redirect('/admin/artikel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Artikel::findOrFail($id);
        ImageManager::deleteImage( public_path() . '/gambar/artikel/' . $data->gambar);
        $data->delete();
        return redirect('/admin/artikel');
    }
}
