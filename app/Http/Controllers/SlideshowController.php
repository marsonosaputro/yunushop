<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Slideshow;
use Image;
use ImageManager;

class SlideshowController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Data Slideshow';
        $data['slideshow'] = Slideshow::orderBy('id', 'desc')->paginate(4);
        $data['no'] = $data['slideshow']->firstItem();
        return view('slideshow.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('slideshow.create', ['page_title'=>'Tambah Slideshow']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['judul'=>'required', 'gambar'=>'required|image']);
        if(!empty($request->file('gambar'))){
            $gambar = time().$request->file('gambar')->getClientOriginalName();
            $request->file('gambar')->move('gambar/slideshow', $gambar);
            Image::make(public_path().'/gambar/slideshow/'.$gambar)->crop(1200,563)->save();
        }else{
            $gambar = '';
        }
        $data = $request->all();
        $data['gambar'] = $gambar;
        Slideshow::create($data);
        return redirect('/admin/slideshow');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Ubah Gambar Slideshow';
        $data['slideshow']  = Slideshow::findOrFail($id);
        return view('slideshow.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['judul'=>'required', 'gambar'=>'image']);
        $data = Slideshow::findOrFail($id);
        if(!empty($request->file('gambar'))){
            $gambar = time().$request->file('gambar')->getClientOriginalName();
            $request->file('gambar')->move('gambar/slideshow', $gambar);
            Image::make(public_path().'/gambar/slideshow/'.$gambar)->crop(1200,563)->save();
            ImageManager::deleteImage( public_path() . '/gambar/slideshow/' . $data->gambar);
        }else{
            $gambar = $data->gambar;
        }
        $slideshow = $request->all();
        $slideshow['gambar'] = $gambar;
        $data->update($slideshow);
        return redirect('/admin/slideshow');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Slideshow::findOrFail($id);
        ImageManager::deleteImage( public_path() . '/gambar/slideshow/' . $data->gambar);
        $data->delete();
        return redirect('/admin/slideshow');
    }
}
