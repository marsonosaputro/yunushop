<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\KategoriArtikel;

class KategoriArtikelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Kategori Artikel';
        $data['kategori'] = KategoriArtikel::all();
        $data['no'] = 1;
        return view('kategoriartikel.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Tambah Kategori Artikel';
        return view('kategoriartikel.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['judul'=>'required|min:5', 'keterangan'=>'required|min:5']);
        $data = $request->all();
        $data['seo'] = str_slug($request->judul, '-');
        KategoriArtikel::create($data);
        return redirect('/admin/kategoriartikel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['kategori'] = KategoriArtikel::findOrFail($id);
        $data['page_title'] = 'Ubah Kategori Artikel';
        return view('kategoriartikel.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['judul'=>'required|min:5', 'keterangan'=>'required|min:5']);
        $kategori = KategoriArtikel::findOrFail($id);
        $data = $request->all();
        $data['seo'] = str_slug($request->judul, '-');
        $kategori->update($data);
        return redirect('/admin/kategoriartikel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        KategoriArtikel::findOrFail($id)->delete();
        return redirect('/admin/kategoriartikel');
    }
}
