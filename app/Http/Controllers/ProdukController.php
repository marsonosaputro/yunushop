<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\SimpanProdukRequest;
use App\Http\Requests\UpdateProdukRequest;
use App\Produk;
use App\Kategori; //manggil model/app Kategori
use Image; //untuk upload
use ImageManager; //untuk menampilkan

class ProdukController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Data Produk Yunus';
        $data['produk']     = Produk::orderBy('id', 'desc')->paginate(10);
        $data['no']         = $data['produk']->firstItem();
        return view('produk.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Tambah Produk';
        $data['kategori'] = kategori::lists('judul', 'id');
        return view('produk.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SimpanProdukRequest $request)
    {
        if(!empty($request->file('gambar'))){
            $gambar = time().$request->file('gambar')->getClientOriginalName();
            $request->file('gambar')->move('gambar/produk/', $gambar);
            $img = Image::make(public_path().'/gambar/produk/'.$gambar)->crop(450,578);
            $img->save();
        }else{
            $gambar = '';
        }

        $produk = $request->all();
        $produk['seo'] = str_slug($request['judul'], '-');
        $produk['gambar'] = $gambar;
        Produk::create($produk);
        return redirect('/admin/produk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Ubah data produk';
        $data['produk'] = Produk::findOrFail($id);
        $data['kategori'] = kategori::lists('judul', 'id');
        return view('produk.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Produk::findOrFail($id);
        if(!empty($request->file('gambar'))){
            $gambar = time().$request->file('gambar')->getClientOriginalName();
            $request->file('gambar')->move('gambar/produk/', $gambar);
            $img = Image::make(public_path().'/gambar/produk/'.$gambar)->crop(450,578);
            $img->save();
            ImageManager::deleteImage( public_path() . '/gambar/produk/' . $data->gambar);
        }else{
            $gambar = $data->gambar;
        }

        $produk = $request->all();
        $produk['seo'] = str_slug($request['judul'], '-');
        $produk['gambar'] = $gambar;
        $data->update($produk);
        return redirect('/admin/produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Produk::findOrFail($id);
        ImageManager::deleteImage( public_path() . '/gambar/produk/' . $data->gambar);
        $data->delete();
        return redirect('/admin/produk');
    }
}
