<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Konfirmasi;
use Session;

class KonfirmasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $rules = [
      'nama' => 'required|min:7',
      'alamat' => 'required|min:10',
      'nohp' => 'required|numeric',
      'bank' => 'required|min:3',
      'rekening' => 'required|numeric',
      'atasnama' => 'required|min:7',
      'nominal' => 'required|numeric'
    ];

    public function __construct()
    {
      $this->middleware('auth', ['except'=>['store', 'create']]);
    }

    public function index()
    {
        $data['page_title'] = 'Konfirmasi';
        $data['retur'] = Konfirmasi::orderBy('id', 'asc')->paginate(10);
        return view('konfirmasi.index', $data)->with('no', $data['retur']->firstItem());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Konfirmasi Pembayaran';
        return view('frontend.konfirmasi', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        Konfirmasi::create($request->all());
        Session::flash('flash_notification', [
                'level'=>'success',
                'message'=>'Konfirmasi berhasil terkirim, terima kasih atas kepercayaannya belanja di tempat kami.'
              ]);
        return redirect(route('konfirmasi.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Konfirmasi::findOrFail($id)->delete();
        return redirect('konfirmasi');
    }
}
