<?php
use App\Http\Controllers\PesananController;

Route::auth();
Route::get('/', 'HomeController@index');
Route::get('/blog/{seo}', 'HomeController@artikel');
Route::get('/blog', 'HomeController@blog');
Route::get('/blog/kategori/{id}', 'HomeController@blogKategori');
Route::get('/cara-belanja', 'HomeController@caraBelanja');
Route::get('/kontak', 'HomeController@kontak');
Route::get('/yunus', 'HomeController@blog');
Route::get('/about', 'HomeController@about');
Route::get('/retur', 'HomeController@retur');
Route::get('/wellcome', 'HomeController@wellcome');
Route::resource('konfirmasi', 'KonfirmasiController');
Route::resource('confirm', 'ReturController');
Route::get('/biaya-ongkir', 'HomeController@biayaongkir');

Route::group(['middleware'=>'auth', 'prefix'=>'admin'], function(){
    Route::resource('statis', 'StatisController');
    Route::resource('produk', 'ProdukController');
    Route::resource('kategori', 'KategoriController');
    Route::resource('slideshow', 'SlideshowController');
    Route::resource('user', 'UserController');
    Route::resource('kategoriartikel', 'KategoriArtikelController');
    Route::resource('artikel', 'ArtikelController');
    Route::resource('ongkir', 'OngkirController');
    Route::get('pesanan', 'PesananController@index');
    Route::get('pesanan/detail/{kode}', 'PesananController@detailOrder');
    Route::post('ubahstatus', 'PesananController@ubahStatus');
    Route::get('laporan', 'LaporanController@index');
    Route::post('hapuspesanan', 'PesananController@hapusPesanan');

});

//CART
Route::get('/gallery-produk', 'TokoController@galleryProduk');
Route::get('/detail-produk/{seo}', 'TokoController@detailproduk');
Route::get('/kategori-produk/{kategori_id}/{seo}', 'TokoController@kategoriproduk');
Route::get('product/cart/{id}', 'TokoController@cart');
Route::get('cart-content', 'TokoController@cartContent');
Route::post('cart-update', 'TokoController@updateCart');
Route::get('cart/delete/{rowid}', 'TokoController@deleteCart');
Route::get('cart/checkout', 'TokoController@checkout');
Route::get('cart/destroy', 'TokoController@destroy');


//USER AREA PELANGGAN
Route::group(['middleware'=>'auth', 'prefix'=>'user'], function(){
  Route::get('cart/account', 'OrderController@account');
  Route::get('cart/edit', 'OrderController@editaccount');
  Route::get('cart/orders', 'OrderController@order');
  Route::get('cart/histori/{master_kode}', 'OrderController@historiorder');
  Route::post('cart/savepelanggan', 'OrderController@savePelanggan');
  Route::get('cart/saveorder', 'OrderController@saveOrder');
  Route::get('cart/ordersukses', 'OrderController@orderSukses');
});


//VIEW
View::composer('frontend', function($view){
  $view->kategoriproduk = App\Kategori::all();
  $view->kategoriartikel = App\KategoriArtikel::all();

});
