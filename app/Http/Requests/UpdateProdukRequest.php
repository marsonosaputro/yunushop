<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateProdukRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'kategori_id'=>'required',
          'judul'=>'required',
          'deskripsi'=>'required',
          'stock'=>'required|integer',
          'harga'=>'required|integer',
          'hargabeli'=>'required|integer',
          'berat'=>'required|integer',
        ];
    }
}
