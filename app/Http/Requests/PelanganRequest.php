<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PelanganRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'unique:pelanggans,user_id',
            'namalengkap'=>'required',
            'alamat'=>'required',
            'kota'=>'required',
            'provinsi'=>'required',
            'kodepos'=>'required',
            'keterangan'=>'required',
            'nohp'=>'required',
            'email'=>'required'
        ];
    }
}
