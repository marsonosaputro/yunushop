<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master extends Model
{
    protected $fillable = ['user_id', 'kode', 'status', 'totalharga'];
}
