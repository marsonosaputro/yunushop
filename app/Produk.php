<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $fillable = ['kategori_id', 'judul', 'seo', 'deskripsi', 'stock', 'harga', 'hargabeli', 'gambar', 'dibaca', 'berat'];
}
