<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statis extends Model
{
    protected $fillable = ['judul', 'seo', 'isi', 'gambar', 'dibaca'];
}
