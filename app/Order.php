<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['master_kode', 'produk_id', 'jumlah', 'harga'];
}
