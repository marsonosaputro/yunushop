<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artikel extends Model
{
    protected $fillable = ['kategori_artikel_id', 'judul', 'seo', 'artikel', 'gambar', 'penulis', 'hits'];
}
