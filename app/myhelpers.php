<?php

//require_once '../public/apifunction.php';

if ( ! function_exists('hari_ini'))
{
	function hari_ini()
	{
		date_default_timezone_set('Asia/Jakarta');
        $seminggu = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
        $hari = date("w");
        $hari_ini = $seminggu[$hari];
        return $hari_ini;
	}
}

if ( ! function_exists('baca_kategori'))
{
	function baca_kategori($id)
	{
		$kat = DB::table('kategoris')->where('id', '=', $id)->first();
		if(!empty($kat)) return $kat->kategori; else return null;
	}
}

if ( ! function_exists('baca_kategori_artikel'))
{
	function baca_kategori_artikel($id)
	{
		$kat = DB::table('kategori_artikels')->where('id', '=', $id)->first();
		if(!empty($kat)) return $kat->judul; else return null;
	}
}

if ( ! function_exists('baca_kategori_downloads'))
{
	function baca_kategori_downloads($id)
	{
		if(!empty($id))
		{
			$kat = DB::table('kat_downloads')->where('id', '=', $id)->first();
			return $kat->kategori;
		}
		return ' ';

	}
}

if ( ! function_exists('tgl_indo'))
{
	function tgl_indo($tanggal)
	{
		$tgl = explode(' ', $tanggal);
		$t   = explode('-', $tgl[0]);
	    switch ($t[1])
	        {
	            case '01'  : $b = 'Jan'; break;
	            case '02'  : $b = 'Feb'; break;
	            case '03'  : $b = 'Mar'; break;
	            case '04'  : $b = 'Apr'; break;
	            case '05'  : $b = 'Mei'; break;
	            case '06'  : $b = 'Jun'; break;
	            case '07'  : $b = 'Jul'; break;
	            case '08'  : $b = 'Agt'; break;
	            case '09'  : $b = 'Sep'; break;
	            case '10'  : $b = 'Okt'; break;
	            case '11'  : $b = 'Nop'; break;
	            case '12'  : $b = 'Des'; break;
	        }
	    $d = $t[2].' '.$b.' '.$t[0];
	    return $d.' | '.$tgl[1];
	}
}

if ( ! function_exists('image'))
{
	function image($x)
	{
		return str_replace('<img', '<img class="img img-responsive img-thumbnail" ', $x);
	}
}

if( ! function_exists('menu'))
{
	function menu($kategori)
	{
		$menu = DB::table('halamanstatis')->where('kategori', $kategori)->take(6)->get();
		return $menu;
	}
}

if( ! function_exists('beritahits'))
{
	function beritahits()
	{
		$data = DB::table('beritas')->orderBy('dibaca', 'desc')->take(6)->get();
		return $data;
	}
}

if(!function_exists('get_tgl'))
{
	function get_tgl($tgl)
	{
		$t = explode('-', $tgl);
		return $t[2];
	}
}

if(! function_exists('getProduk'))
{
	function getProduk($id)
	{
		$produk = DB::table('produks')->where('id', '=', $id)->first();
		return $produk->judul;
	}
}

if(!function_exists('getNama'))
{
	function getNama($id)
	{
		$nama = DB::table('users')->where('id', '=', $id)->first();
		return $nama->name;
	}
}
