-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Inang: localhost:3306
-- Waktu pembuatan: 10 Nov 2016 pada 08.53
-- Versi Server: 5.5.50-cll
-- Versi PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `butikani_toko`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `artikels`
--

CREATE TABLE IF NOT EXISTS `artikels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kategori_artikel_id` int(11) NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `artikel` text COLLATE utf8_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `penulis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hits` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `artikels`
--

INSERT INTO `artikels` (`id`, `kategori_artikel_id`, `judul`, `seo`, `artikel`, `gambar`, `penulis`, `hits`, `created_at`, `updated_at`) VALUES
(1, 5, 'Merawat Batik', 'merawat-batik', '<p>Berikut adalah proses Tips Merawat Batik Tulis selengkapnya :<br />\r\n1. Tidak boleh mencuci kain/ baju batik tulis dengan menggunakan mesin cuci. Sebab kain/ baju batik tulis perlu penanganan ekstra hati&shy;hati, maka cuci saja dengan&nbsp;menggunakan tangan. Serta hindari penggunaan deterjen, gantilah dengan sampo atau sabun. Cuci secara perlahan&shy;lahan kemudian campur gunakan air hangat&nbsp;saat merendam kain batik tulis.<br />\r\n2. Saat kain/ baju batik tulis dijemur, usahakanlah agar tidak terkena pancaran sinar matahari secara langsung cukup dengan diangin&shy;angin saja. Saat disetrika,&nbsp;usahakan menggunakan kain pelapis, maksudnya supaya kain/ baju batik tulis tidak langsung menempel dengan setrika panas.<br />\r\n3. Saat kain/ baju batik tulis hendak disimpan, perlu penempatan yang tepat, lipatlah kain batik tulis kemudian bungkus dengan kertas minyak atau plastik pembungkus.&nbsp;Jangan membungkus kain batik tulis menggunakan koran, dikuatirkan tinta tulisan pada koran akan menodai kain batik tulis. Serta jangan menggunakan kapur barus&nbsp;dalam lemari yang dijadikan tempat menyimpan kain batik tulis karena bisa merusak warna kain batik.<br />\r\n4. Yang terakhir, upayakan kain batik tulis yang disimpan dalam lemari dibuka setiap dua minggu sekalli, lalu diangin&shy;angin selama 10 sampai 15 menit untuk mengusir&nbsp;bau apek yang biasa terdapat dalam lemari karena suhu yang lembap di dalamnya.<br />\r\nCara perawatan kain/ baju Batik Cap, hampir sama dengan cara merawat batik tulis, karena cara pembuatannya juga hampir sama. Berikut kami berikan Tips Merawat Batik&nbsp;Cap yang benar dan mudah :<br />\r\n1. Hindari penggunaan sabun deterjen, karena sifatnya terlalu keras dalam mengikis warna yang menempel pada kain. Saran kami, yang paling murah meriah, cukup&nbsp;gunakan shampoo rambut untuk mencuci kain batik atau produk&shy;produk khusus yang saat ini sudah banyak dijual di toko maupun di gallery batik (klerak cair, m**to<br />\r\nsilk, dll).<br />\r\n2. Saat mencuci kain/ baju batik cap, jangan menyikatnya, cukup dikucek&shy;kucek lembut, terutama bagian&shy;bagian yang kotor saja (misal : bagian kerah).<br />\r\n3. Saat mencuci kain/ baju batik cap, tidak perlu diperas. Hindari juga penggunaan mesin cuci. Cukup ditarik bagian ujung&shy;ujungnya saja agar kain batik lurus kembali.<br />\r\n4. Hindari menjemur kain/baju batik di tempat yang langsung terkena sinar matahari, cukup dijemur ditempat teduh atau diangin&shy;anginkan saja.<br />\r\n5. Hindari menyeterika langsung kain/baju batik. Saran kami, seterika bagian dalamnya saja atau kalau memang terlalu kusut, lapisi dengan kain bersih lain sebelum&nbsp;diseterika.<br />\r\n6. Hindari semprot langsung kain/baju batik dengan parfum atau pengharum badan yang lainnya, karena ini akan merusak warna pada corak batik Anda.<br />\r\n7. Pada saat menyimpan kain/baju batik di lemari, hindari menggunakan kapur barus dan sejenisnya, karena akan merusak kain. Saran kami, gunakan merica dan atau&nbsp;lada, bungkus dengan tissue dan taruh di pojok&shy;pojok lemari Anda. Dijamin, ngengat dan sejenisnya tidak akan mendekat.<br />\r\nSedangkan cara perawatan kain/ baju Batik Printing sedikit berbeda dengan kedua jenis kain/ baju batik sebelumnya. Diperlukan perawatan khusus agar kain/ baju batik&nbsp;printing Anda tetap bisa awet dan terus dikenakan. Perhatikan Tips Perawatan Batik Printing &nbsp;berikut ini:<br />\r\n1. Memakai shampo Larutkan shampo ke dalam air hingga tidak ada lagi shampo yang mengental. Rendam kain batik printing selama 10 atau 15 menit saja. Kucek&nbsp;perlahan dan bilas hingga bersih.<br />\r\n2. Hindari mesin cuci Setelah merendam batik printing, jangan gunakan mesin untuk mencuci. Sebaiknya cuci dengan menggunakan tangan. Mencuci dengan mesin&nbsp;malah akan membuat warnanya lebih cepat pudar. Noda makanan pada batik printing bisa dihilangkan dengan menggunakan kulit jeruk, cukup diusap saja<br />\r\nbeberapa kali.<br />\r\n3. Menjemur terbalik Sinar matahari dapat membuat warna batik printing lebih cepat pudar. Kain batik printing tidak perlu diperas dan jemur ditempat yang teduh&nbsp;sehingga tidak terpapar sinar matahari langsung. Jemur bagian dalam menghadap keluar agar warna batik printing tidak pudar.<br />\r\n4. Melapisi batik sebelum diberi pewangi Ketika kain batik sudah kering, lapisi dengan kain putih atau polos sebelum menyemprotnya dengan pelicin atau pelembut&nbsp;kain. Ini untuk menghindari zat kimia dari pewangi yang bisa merusak warna batik.<br />\r\n5. Lapisi kain saat disetrika Setelah diberi pewangi atau pelembut, mulailah menyetrika kain batik dengan kain polos sebagai lapisan atasnya sehingga panas dari&nbsp;setrika tidak langsung bersentuhan dengan kain. Ini akan menjaga agar batik tidak cepat pudar.<br />\r\n6. Merica dalam lemari Kain batik printing yang telah dicuci bersih dan disetrika ini bisa disimpan dalam lemari dan beri sedikit merica. Masukkan merica dalam tissu,&nbsp;lipat dan letakkan di dekat kain batik tadi. Jangan menggunakan kapur barus karena bisa merusak kain.<br />\r\n7. Hindari parfum Memakai wewangian seperti parfum saat memakai batik tidak dianjurkan. Kandungan pewarna alami pada batik printing bisa rusak karena&nbsp;kandungan kimiawi parfum yang bersentuhan langsung dengan warnanya.<br />\r\nItu dia tujuh langkah yang perlu dilakukan agar batik bisa tetap awet dan tidak pudar. Semua cara itu bisa dilakukan dengan mudah agar batik kesayangan jadi lebih awet.&nbsp;Merawat kain batik memang gampang&shy;gampang susah.&nbsp;Demikianlah, sekilas tips merawat kain batik tulis, cap, dan printing semoga bisa bermanfaat bagi Anda. Jika tips&shy;tips tersebut Anda lakukan, insya Alloh kain/ baju batik Anda&nbsp;akan berumur lebih lama (awet baik kainnya maupun kualitas warnanya).&nbsp;</p>\r\n', '1473995545unduhan.jpg', 'Administrator', 0, '2016-06-17 23:26:57', '2016-09-16 03:12:25'),
(2, 4, 'Membuat Batik', 'membuat-batik', '<p>Tak akan ada habisnya jika membahas tentang kebudayaan Indonesia yang sangat beragam. Kerajinan batik<br />\r\nadalah salah satu identitas dan kebanggaan bangsa Indonesia. Oleh karena itu, sebagai pemilik identitas<br />\r\ntersebut sudah seharusnya mengetahui seluk beluk dari kerajinan batik itu sendiri, seperti mengetahui<br />\r\ntentang motif-motif batik,&nbsp;daerah-daerah sentra batik, ciri khas batik tiap daerah, dan proses<br />\r\npembuatannya. Pada umumnya, proses pembuatan batik di setiap daerah itu sama. Di daerah-daerah<br />\r\nsentra batik pasti tak sedikit turis asing mengunjungi workshop&Acirc; &nbsp;membatik untuk mengetahui secara<br />\r\nlangsung proses pembuatan batik tersebut, bahkan tak jarang dari mereka mencoba merasakan proses<br />\r\nmembatik. Dengan demikian, sebagai pemilik kita tidak boleh dilangkahi oleh pendatang. Maka dari itu,<br />\r\njangan segan untuk belajar memahami kebudayaan sendiri. dalam hal ini, mari kita mencoba mengenal<br />\r\nbagaimana cara membuat batik. Berikut langkah-langkah untuk membuat batik:<br />\r\n1. Siapkan alat dan bahan untuk membatik seperti: kain mori sesuai kebutuhan yang telah diketel (proses<br />\r\nmenghilangkan kanji pada kain dengan cara diuleni dalam larutan minyak kacang) dan canting.<br />\r\n2. Gambar desain di atas kain mori sesuai dengan pola yang diinginkan. Dalam istilah perbatikan tahap ini<br />\r\nsering disebut Nglengreng.<br />\r\n3. Panaskan lilin/malem diatas wajan hingga mencair sempurna. Suhu maksimal lilin/ malem sekitar 80<br />\r\nderajat Celcius. Jadi, harus berhati-hati saat menggunakannya.<br />\r\n4. Perhatikan posisi duduk saat membatik. Duduklah dengan posisi tungku/ kompor batik berada di sebelah<br />\r\nkanan(kecuali kidal, tungku/ kompor ada di sebelah kiri) untuk memudahkan mengambil malem dan<br />\r\nmenggoreskannya ke atas kain mori.</p>\r\n\r\n<p>5. Celupkan canting ke dalam wajan yang terisi oleh malem selama sekitar 3 detik sebagai pengesuaian<br />\r\nsuhu pada canting.<br />\r\n6. Mulailah menggoreskan canting ke atas kain yang telah dilengreng (dipola) dengan menggoreskannya<br />\r\ndari kiri ke kanan sama halnya dengan menulis latin. Hal ini dimaksudkan agar mendapatkan goresan yang<br />\r\nbaik dan halus.<br />\r\n7. Isilah bagian pola yang kosong dengan ornamen-ornamen seperti garis-garis arsiran maupun titik-titik.<br />\r\nMisalnya pada gambar daun mestinya memiliki tulang daun, maka daun tersebut akan diisi garis sesuai<br />\r\ndengan kebutuhan. Tahap ini biasa disebut dengan istilah Isen-isen.<br />\r\n8. Tahap nembok artinya mengeblok bagian kain yang tidak ingin terkena warna. Namun, tahap ini<br />\r\ndilakukan apabila dibutuhkan warna awalnya.<br />\r\n9. Tahap pencelupan warna. Biasanya menggunakan pewarna sintesis napthol dan indigosol. diperlukan<br />\r\nbeberapa kali celupan untuk memunculkan warnanya.<br />\r\n10. Tiriskan kain yang telah dicelup dan diamkan agar warnanya dapat meresap dengan maksimal pada<br />\r\nserat kain.<br />\r\n11. Rebus kain dalam air mendidih 100 derajat Celcius untuk melirihkan lilin/ malem yang menempel pada<br />\r\nkain untuk memunculkan motif yang telah didisain. tahap merebus ini disebut nglorod.<br />\r\n12. Cuci kain batik dengan air bersih untuk menghilangkan sisa-sisa lilin/ malem yang masih menempel.<br />\r\nKemudian, jemurlah dengan angin-angin dan hindari terkena panas sinar matahari langsung.<br />\r\nDengan mengetahui dan bahkan mencoba untuk membatik, hal tersebut termasuk upaya kita dalam<br />\r\nmelestarikan kebudayaan yang telah menjadi identitas bangsa Indonesia. Oleh karena itu, langkah-langkah<br />\r\nmembuat batik tersebut dapat membantu Anda untuk mencobanya. Akan tetapi, Anda tetap harus berhati-<br />\r\nhati jika Anda seorang pemula atau baru belajar membatik. Sebaiknya Anda didampingi oleh ahlinya<br />\r\nterlebih dahulu agar keselamatan Anda lebih diperhatikan.</p>\r\n', '1473995152cara-membuat-batik-tulis.jpg', 'Administrator', 0, '2016-06-17 23:27:20', '2016-09-16 03:05:53'),
(3, 2, 'Kisah Sukses Bisnis Batik', 'kisah-sukses-bisnis-batik', '<p>Namanya Ibnu Riyanto asal Cirebon, Jawa Barat. Usianya belum&nbsp;menginjak 30 tahun, namun sudah cukup sukses sebagai pengusaha batik. Keberhasilan&nbsp;Ibnu dibuktikan dengan berbagai penghargaan dan pencapaian usaha batik yang&nbsp;dimilikinya.<br />\r\nSalah satu penghargaan yang sangat berkesan berasal dari Museum Rekor Indonesia&nbsp;(MURI) sebagai pemilik toko batik terbesar dan terluas pada usia termuda (23 tahun).&nbsp;Meski sudah meraih kesuksesan di usia muda, Ibnu tak langsung besar kepala. &quot;Saya tidak&nbsp;mau seperti dinosaurus, ketika sudah besar kemudian punah,&quot; ungkapnya saat ditemui&nbsp;team Berita BCA belum lama ini di toko Batik Trusmi, Desa Trusmi, Plered, Cirebon.<br />\r\nBagi Ibnu, usia muda adalah kesempatan untuk terus melakukan eksplorasi kemampuan&nbsp;diri dan semakin tertantang untuk meraih yang belum bisa dicapainya.&nbsp;&quot;Mumpung masih muda dan masih cukup banyak energi, saya akan terus&nbsp;mengembangkan diri. Saya memang tipe orang yang tidak mudah puas dengan apapun&nbsp;yang sudah saya capai,&quot; kata Ibnu.&nbsp;Meski berasal dari keluarga pembatik, Ibnu membangun usahanya sendiri benar-benar&nbsp;dari nol. Ibnu mulai merintis usaha tahun 2006 ketika usianya 17 tahun dengan menjadi<br />\r\nsuplier kain mori yakni bahan baku batik berupa kain putih.&nbsp;Dengan modal awal Rp 15 juta, Ibnu menawarkan kain mori ke perajin-perajin batik di desa&nbsp;kelahirannya, Trusmi Kabupaten Cirebon.&nbsp;&quot;Dulu saya bandel. Lulus SMA saya langsung menikah. Saya ingin membuktikan kepada&nbsp;orang tua kalau saya mampu mandiri. Begitu punya tanggungan istri, saya jadi&nbsp;bersemangat untuk memulai usaha sendiri. Saat itulah, pertama kali saya menjadi nasabah&nbsp;bank ya BCA,&quot; katanya.&nbsp;Keuntungan menjadi suplier kain mori hanya cukup untuk kebutuhan keluarganya sehari-&nbsp;hari, tidak lebih dan tidak kurang. Setelah mempunyai anak, Ibnu merasa harus bisa&nbsp;meningkatkan usahanya untuk menghidupi keluarga kecilnya.&nbsp;Memanfaatkan ruang tamu rumah orang tuanya yang berukuran 4 x 4 meter persegi di&nbsp;tahun 2007, Ibnu yang memiliki 2 anak ini pun mulai menjual batik. Tak berhenti di &quot;toko&quot;&nbsp;saja, Ibnu pun gigih memasarkan batik dagangannya secara door to door dari satu toko ke toko yang lain.</p>\r\n\r\n<p><strong>Selanjutnya perjalanan Ibnu melebarkan usahanya</strong><br />\r\nKetika memasarkan batik dagangannya, tak jarang Ibnu harus tidur di masjid demi mengirit&nbsp;uang yang harus diputarnya untuk mengembangkan usaha. Ya, meraih sukses memang tak&nbsp;semudah membalikkan tangan butuh perjuangan dan pengorbanan.<br />\r\nBeruntung satu toko di salah satu pusat perbelanjaan teramai di Jakarta mau membayar&nbsp;lunas dagangannya sebesar Rp25 juta. Pencapaian itu membuatnya semakin bersemangat&nbsp;dan percaya diri hingga semakin ulet memasarkan batiknya.<br />\r\nSeiring dengan semakin laris dagangan batiknya, Ibnu pun membuka usaha konveksi&nbsp;sendiri dan berkat&nbsp;ketekunannya, Ibnu mampu membuka toko batik yang diberinya nama&nbsp;Batik Trusmi mengikuti nama desa penghasil batik ternama di Cirebon. Usahanya&nbsp;membangun bisnis akhirnya mendapat penghargaan rekor MURI pada 25 Maret 2013 lalu.<br />\r\nSebagai pengusaha, Ibnu memiliki pesan kepada masyarakat diseluruh Indonesia bahwa<br />\r\nkalau usaha ingin berkembang manajemen yang profesional saja tidak cukup.&nbsp;Alamat email&nbsp;contoh: nama@mail.com&nbsp;&quot;Tapi juga diperlukan kerja keras dan kemauan untuk maju serta berani mengambil resiko.&nbsp;Dan tentu saja, rasa selalu ingin mencapai yang lebih lagi,&quot; papar Ibnu.&nbsp;Ikuti kami di media sosial.&nbsp;Saat ini Ibnu mampu menghidupi setidaknya 500 orang karyawan dan membuka&nbsp;kesempatan bagi sedikitnya 50 perajin batik rumahan untuk memasarkan batik di toko&nbsp;miliknya.<br />\r\nIbnu Riyanto bukan hanya dikenal sebagai pemilik toko &quot;Pusat Grosir Batik Trusmi&quot; dengan&nbsp;luas hampir 9.000 meter persegi di atas tanah seluas 12.000 meter persegi.<br />\r\nTapi Ibnu juga membuka gerai batik untuk kelas menengah ke atas yang diberi nama&nbsp;&quot;Pesona Batik&quot;, sebuah gerai batik memadukan seni budaya dan keindahan gedung&nbsp;peninggalan sejarah. Pesona Batik ini pun diresmikan oleh Wakil Gubernur Jawa Barat&nbsp;Dedi Mizwar pada 27 Maret 2014 lalu.<br />\r\nTidak berhenti disitu, Ibnu juga membuka sejumlah toko batik dan puluhan gerai batik di&nbsp;sejumlah mal baik di Cirebon maupun kota lainnya. Untuk mereka yang gemar berbelanja&nbsp;via online, Ibnu juga membuka website www.batiktrusmi.com yang dipopulerkan juga melalui sosial media. Insting bisnisnya terus diasah. Saat ini, Ibnu pun melebarkan sayap&nbsp;bisnisnya dengan terjun ke dunia properti.<br />\r\nSebagai pebisnis, Ibnu juga menggandeng perbankan untuk mendukung usahanya. BCA&nbsp;menjadi salah satu bank pilihannya. Salah satunya menggunakan EDC (Electronic Data&nbsp;Capture) BCA untuk kemudahan transaksi di seluruh toko dan gerainya serta&nbsp;menggunakan KlikBCA Bisnis yang praktis untuk menyelesaikan berbagai transaksi&nbsp;bisnisnya.<br />\r\nKeinginan Ibnu selanjutnya adalah mengumpulkan perajin batik dalam satu kawasan&nbsp;seperti halnya pabrik sehingga bisa memberikan imbalan kepada perajin minimal setara&nbsp;dengan UMK bahkan lebih. &quot;Upah membatik yang masih rendah, ikut memberikan andil&nbsp;ancaman kepunahan batik di Cirebon,&quot; kata Ibnu.<br />\r\nTerinspirasi dengan kisah sukses Ibnu Riyanto? Maka, jangan pernah berhenti untuk&nbsp;mengeksplor kemampuan diri Anda, miliki kemauan keras untuk maju dan berani&nbsp;mengambil resiko seperti yang pernah dilakukan Ibnu Riyanto.</p>\r\n', '1473994931Dea-Valensia-Budiarto-Gadis-Belia-yang-Sukses-Bisnis-Batik-Kultur.jpg', 'Administrator', 0, '2016-06-22 00:01:11', '2016-09-16 03:02:11'),
(4, 3, 'Sejarah Batik', 'sejarah-batik', '<p><strong>Pengertian Batik</strong><br />\r\nBatik merupakan budaya yang telah lama berkembang dan dikenal oleh masyarakat Indonesia. Kata batik mempunyai beberapa pengertian. Menurut&nbsp;Hamzuri dalam bukunya yang berjudul Batik Klasik, pengertian batik merupakan suatu cara untuk memberi hiasan pada kain dengan cara menutupi bagian-bagian tertentu dengan menggunakan perintang. Zat perintang yang sering digunakan ialah lilin atau malam.kain yang sudah digambar dengan&nbsp;menggunakan malam kemudian diberi warna dengan cara pencelupan.setelah itu malam dihilangkan dengan cara merebus kain. Akhirnya dihasilkan sehelai&nbsp;kain yang disebut batik berupa beragam motif yang mempunyai sifat-sifat khusus.<br />\r\nSecara etimologi kata batik berasal dari bahasa Jawa, yaitu&rdquo;tik&rdquo; yang berarti titik / matik (kata kerja, membuat titik) yang kemudian berkembang menjadi&nbsp;istilah &rdquo;batik&rdquo; (Indonesia Indah &rdquo;batik&rdquo;, 1997, 14). Di samping itu mempunyai pengertian yang berhubungan dengan membuat titik atau meneteskan malam&nbsp;pada kain mori. Menurut KRT.DR. HC. Kalinggo Hanggopuro (2002, 1-2) dalam buku Bathik sebagai Busana Tatanan dan Tuntunan menuliskan bahwa, para&nbsp;penulis terdahulu menggunakan istilah batik yang sebenarnya tidak ditulis dengan kata&rdquo;Batik&rdquo; akan tetapi seharusnya&rdquo;Bathik&rdquo;. Hal ini mengacu pada huruf&nbsp;Jawa &rdquo;tha&rdquo; bukan &rdquo;ta&rdquo; dan pemakaiaan bathik sebagai rangkaian dari titik adalah kurang tepat atau dikatakan salah. Berdasarkan etimologis tersebut&nbsp;sebenarnya batik identik dikaitkan dengan suatu teknik (proses) dari mulai penggambaran motif hingga pelorodan. Salah satu yang menjadi ciri khas dari&nbsp;batik adalah cara pengambaran motif pada kain ialah melalui proses pemalaman yaitu mengoreskan cairan lilin yang ditempatkan pada wadah yang<br />\r\nbernama canting dan cap.<br />\r\n&nbsp;<br />\r\n<strong>Sejarah Perkembangan Batik</strong><br />\r\nDitinjau dari perkembangan, batik telah mulai dikenal sejak jaman Majapahit dan masa penyebaran Islam. Batik pada mulanya hanya dibuat terbatas oleh&nbsp;kalangan keraton. Batik dikenakan oleh raja dan keluarga serta pengikutnya. Oleh para pengikutnya inilah kemudian batik dibawa keluar keraton dan&nbsp;berkembang di masyarakat hingga saat ini. Berdasarkan sejarahnya, periode perkembangannya batik dapat dikelompokkan sebagai berikut :<br />\r\n&nbsp;<br />\r\n<em>Jaman Kerajaan Majapahit</em><br />\r\nBerdasarkan sejarah perkembangannya, batik telah berkembang sejak jaman Majapahit. Mojokerto merupakan pusat kerajaan Majapahit dimana batik telah&nbsp;dikenal pada saat itu. Tulung Agung merupakan kota di Jawa Timur yang juga tercatat dalam sejarah perbatikan. Pada waktu itu, Tulung Agung masih berupa<br />\r\nrawa-rawa yang dikenal dengan nama Bonorowo, dikuasai oleh Adipati Kalang yang tidak mau tunduk kepada Kerajaan Majapahit hingga terjadilah aksi&nbsp;polisionil yang dilancarkan oleh Majapahit. Adipati Kalang tewas dalam pertempuran di sekitar desa Kalangbret dan Tulung Agung berhasil dikuasai oleh&nbsp;Majapahit. Kemudian banyak tentara yang tinggal di wilayah Bonorowo (Tulung Agung) dengan membawa budaya batik. Merekalah yang mengembangkan&nbsp;batik. Dalam perkembangannya, batik Mojokerto dan Tulung Agung banyak dipengaruhi oleh batik Yogyakarta. Hal ini terjadi karena pada waktu clash tentara&nbsp;kolonial Belanda dengan pasukan Pangeran Diponegoro, sebagian dari pasukan Kyai Mojo mengundurkan diri ke arah timur di daerah Majan. Oleh karena&nbsp;itu, ciri khas batik Kalangbret dari Mojokerto hampir sama dengan batik Yogyakarta, yaitu dasarnya putih dan warna coraknya coklat muda dan biru tua.<br />\r\n<em>Jaman Penyebaran Islam</em><br />\r\nBatoro Katong seorang Raden keturunan kerajaan Majapahit membawa ajaran Islam ke Ponorogo, Jawa Timur. Dalam perkembangan Islam di Ponorogo&nbsp;terdapat sebuah pesantren yang berada di daerah Tegalsari yang diasuh Kyai Hasan Basri. Kyai Hasan Basri adalah menantu raja Kraton Solo. Batik yang kala<br />\r\nitu masih terbatas dalam lingkungan kraton akhirnya membawa batik keluar dari kraton dan berkembang di Ponorogo. Pesantren Tegalsari mendidik anak&nbsp;didiknya untuk menguasai bidang-bidang kepamongan dan agama. Daerah perbatikan lama yang dapat dilihat sekarang adalah daerah Kauman yaitu&nbsp;Kepatihan Wetan meluas ke desa Ronowijoyo, Mangunsuman, Kertosari, Setono, Cokromenggalan, Kadipaten, Nologaten, Bangunsari, Cekok, Banyudono dan&nbsp;Ngunut.</p>\r\n\r\n<p><em>Batik Solo dan Yogyakarta</em><br />\r\nBatik di daerah Yogyakarta dikenal sejak jaman Kerajaan Mataram ke-I pada masa raja Panembahan Senopati. Plered merupakan desa pembatikan pertama.&nbsp;Proses pembuatan batik pada masa itu masih terbatas dalam lingkungan keluarga kraton dan dikerjakan oleh wanita-wanita pengiring ratu. Pada saat<br />\r\nupacara resmi kerajaan, keluarga kraton memakai pakaian kombinasi batik dan lurik. Melihat pakaian yang dikenakan keluarga kraton, rakyat tertarik dan&nbsp;meniru sehingga akhirnya batikan keluar dari tembok kraton dan meluas di kalangan rakyat biasa.&nbsp;Ketika masa penjajahan Belanda, dimana sering terjadi peperangan yang menyebabkan keluarga kerajaan yang mengungsi dan menetap di daerah-daerah&nbsp;lain seperti Banyumas, Pekalongan, dan ke daerah timur Ponorogo, Tulung Agung dan sebagainya maka membuat batik semakin dikenal di kalangan luas.<br />\r\n<em>Batik di Wilayah Lain</em><br />\r\nPerkembangan batik di Banyumas berpusat di daerah Sokaraja. Pada tahun 1830 setelah perang Diponegoro, batik dibawa oleh pengikut-pengikut Pangeran&nbsp;Diponegoro yang sebagian besar menetap di daerah Banyumas. Batik Banyumas dikenal dengan motif dan warna khusus dan dikenal dengan batik&nbsp;Banyumas. Selain ke Banyumas, pengikut Pangeran Diponegoro juga ada yang menetap di Pekalongan dan mengembangkan batik di daerah Buawaran,&nbsp;Pekajangan dan Wonopringgo.<br />\r\nSelain di daerah Jawa Tengah, batik juga berkembang di Jawa Barat. Hal ini terjadi karena masyarakat dari Jawa Tengah merantau ke kota seperti Ciamis dan&nbsp;Tasikmalaya. Daerah pembatikan di Tasikmalaya adalah Wurug, Sukapura, Mangunraja dan Manonjaya. Di daerah Cirebon batik mulai berkembang dari<br />\r\nkeraton dan mempunyai ciri khas tersendiri.</p>\r\n', '1473964342sejarah-batik-solo-1.jpg', 'Administrator', 0, '2016-06-22 00:05:38', '2016-09-15 18:32:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategoris`
--

CREATE TABLE IF NOT EXISTS `kategoris` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `kategoris`
--

INSERT INTO `kategoris` (`id`, `judul`, `seo`, `keterangan`, `created_at`, `updated_at`) VALUES
(2, 'Daster', 'daster', 'Keterangan kategori daster', '2016-06-05 23:56:06', '2016-08-17 23:26:38'),
(3, 'Jarik', 'jarik', 'Ketersngan judul jarik', '2016-06-05 23:56:28', '2016-08-17 23:28:22'),
(4, 'KImono', 'kimono', 'Keteragan kimono', '2016-08-17 23:28:37', '2016-08-17 23:28:37'),
(5, 'Sprei', 'sprei', 'Keterangan Sprei', '2016-08-17 23:28:49', '2016-09-14 00:56:21'),
(6, 'Mukena', 'mukena', 'Keterangan mukena', '2016-08-17 23:29:03', '2016-08-17 23:29:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_artikels`
--

CREATE TABLE IF NOT EXISTS `kategori_artikels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `seo` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `kategori_artikels`
--

INSERT INTO `kategori_artikels` (`id`, `judul`, `seo`, `keterangan`, `created_at`, `updated_at`) VALUES
(2, 'Bisnis', 'bisnis', 'Artikel tentang bisnis batik', '2016-06-14 08:17:53', '2016-08-28 10:22:30'),
(3, 'Sejarah', 'sejarah', 'Artikel tentang sejarah batik', '2016-06-15 07:06:10', '2016-08-28 10:22:19'),
(4, 'Pendidikan', 'pendidikan', 'Artikel tentang pendidikan membuat batik', '2016-08-28 10:22:56', '2016-08-28 10:22:56'),
(5, 'Pengetahuan', 'pengetahuan', 'Artikel tentang pengetahuan merawat batik', '2016-08-28 10:26:07', '2016-08-28 10:26:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `konfirmasis`
--

CREATE TABLE IF NOT EXISTS `konfirmasis` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `nohp` varchar(15) NOT NULL,
  `bank` varchar(30) NOT NULL,
  `rekening` varchar(20) NOT NULL,
  `atasnama` varchar(100) NOT NULL,
  `nominal` varchar(12) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `konfirmasis`
--

INSERT INTO `konfirmasis` (`id`, `nama`, `alamat`, `nohp`, `bank`, `rekening`, `atasnama`, `nominal`, `created_at`, `updated_at`) VALUES
(1, 'Marsono', 'Karangpung', '0865434556', 'BCA', '1152345', 'Marsono', '120000', '2016-10-20 20:29:35', '2016-10-20 20:29:35'),
(2, 'Agud Triyanto', 'Karangmalang Sragen', '8876543455', 'BCA', '23213445', 'Agus Tri', '120000', '2016-10-20 20:36:06', '2016-10-20 20:36:06'),
(5, 'nama lengkap', 'alamat alamat', '0987654321234', 'BCA', '89798809', 'atas nama ', '41412211', '2016-10-24 06:08:22', '2016-10-24 06:08:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `masters`
--

CREATE TABLE IF NOT EXISTS `masters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `totalharga` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `masters`
--

INSERT INTO `masters` (`id`, `user_id`, `kode`, `totalharga`, `status`, `created_at`, `updated_at`) VALUES
(8, 12, '1474372808', 224000, 'baru', '2016-09-20 05:00:08', '2016-09-20 05:00:08'),
(7, 11, '1474362740', 314000, 'baru', '2016-09-20 02:12:20', '2016-09-20 02:12:20'),
(5, 11, '1474353018', 150000, 'terkirim', '2016-09-19 23:30:18', '2016-09-19 23:30:18'),
(9, 15, '1477016239', 400000, 'baru', '2016-10-20 19:17:19', '2016-10-20 19:17:19'),
(10, 15, '1477038705', 180000, 'baru', '2016-10-21 01:31:45', '2016-10-21 01:31:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_08_16_123711_create_artikels_table', 1),
('2016_08_16_123810_create_kategori_artikels_table', 1),
('2016_08_16_123841_create_kategoris_table', 1),
('2016_08_16_123911_create_produks_table', 1),
('2016_08_16_123928_create_slideshows_table', 1),
('2016_08_16_123946_create_statis_table', 1),
('2016_08_18_070100_create_pelanggans_table', 1),
('2016_08_18_070213_create_orders_table', 1),
('2016_08_24_115852_create_masters_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_08_16_123711_create_artikels_table', 1),
('2016_08_16_123810_create_kategori_artikels_table', 1),
('2016_08_16_123841_create_kategoris_table', 1),
('2016_08_16_123911_create_produks_table', 1),
('2016_08_16_123928_create_slideshows_table', 1),
('2016_08_16_123946_create_statis_table', 1),
('2016_08_18_070100_create_pelanggans_table', 1),
('2016_08_18_070213_create_orders_table', 1),
('2016_08_24_115852_create_masters_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ongkirs`
--

CREATE TABLE IF NOT EXISTS `ongkirs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kota` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `ongkirs`
--

INSERT INTO `ongkirs` (`id`, `kota`, `harga`, `created_at`, `updated_at`) VALUES
(2, 'Surabaya', 10000, '2016-10-26 06:32:39', '2016-10-27 08:31:05'),
(3, 'Yogyakarta', 9000, '2016-10-26 06:32:53', '2016-10-27 08:30:11'),
(4, 'Semarang', 9000, '2016-10-26 06:33:28', '2016-10-27 08:29:38'),
(5, 'Bandung', 14000, '2016-10-26 06:33:46', '2016-10-27 08:28:56'),
(6, 'Jakarta', 11000, '2016-10-26 06:50:27', '2016-10-27 08:26:26'),
(7, 'Makassar', 29000, '2016-10-27 08:32:29', '2016-10-27 08:32:29'),
(8, 'Medan', 31000, '2016-10-27 08:32:56', '2016-10-27 08:32:56'),
(9, 'Samarinda', 29000, '2016-10-27 08:34:35', '2016-10-27 08:34:35'),
(10, 'Pontianak', 27000, '2016-10-27 08:35:50', '2016-10-27 08:35:50'),
(11, 'Bogor', 14000, '2016-10-27 08:37:05', '2016-10-27 08:37:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `master_kode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `produk_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data untuk tabel `orders`
--

INSERT INTO `orders` (`id`, `master_kode`, `produk_id`, `jumlah`, `harga`, `created_at`, `updated_at`) VALUES
(9, '1474362740', 36, 1, 225000, '2016-09-20 02:12:20', '2016-09-20 02:12:20'),
(5, '1474353018', 4, 2, 75000, '2016-09-19 23:30:18', '2016-09-19 23:30:18'),
(8, '1474362740', 28, 1, 89000, '2016-09-20 02:12:20', '2016-09-20 02:12:20'),
(10, '1474372808', 28, 1, 89000, '2016-09-20 05:00:08', '2016-09-20 05:00:08'),
(11, '1474372808', 16, 1, 135000, '2016-09-20 05:00:08', '2016-09-20 05:00:08'),
(12, '1477016239', 52, 2, 200000, '2016-10-20 19:17:19', '2016-10-20 19:17:19'),
(13, '1477038705', 51, 1, 180000, '2016-10-21 01:31:45', '2016-10-21 01:31:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('sabilillahgroup@gmail.com', 'ed9d849742c621b40fcdd418fbc05447f0e08cb989f1f3283399a7688344576a', '2016-09-20 02:08:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelanggans`
--

CREATE TABLE IF NOT EXISTS `pelanggans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `namalengkap` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kota` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provinsi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kodepos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nohp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `pelanggans`
--

INSERT INTO `pelanggans` (`id`, `user_id`, `namalengkap`, `alamat`, `kota`, `provinsi`, `kodepos`, `keterangan`, `nohp`, `email`, `created_at`, `updated_at`) VALUES
(1, 4, 'nova', 'garen', 'boyolali', 'jawa tengah', '57375', '-', '0856789765', 'no@gmail.com', '2016-08-24 20:03:23', '2016-08-24 20:03:23'),
(2, 5, 'yunusw', 'garen', 'boyolali', 'jateng', '65774', '-', '0908907043', 'yun@gmail.com', '2016-08-24 20:31:22', '2016-08-24 20:31:22'),
(3, 2, 'M. YUsron Adnan', 'Karangpung RT 01/09', 'Boyolali', 'Jawa Tengah', '57375', '-', '085742324183', 'yusron@gmail.com', '2016-08-30 05:54:24', '2016-08-30 05:54:24'),
(4, 6, 'Yunusn', 'garen', 'boyolali', 'jateng', '57375', '-', '08765432123', 'y@gmail.com', '2016-08-30 06:27:43', '2016-08-30 06:27:43'),
(5, 7, 'yunusno', 'garen', 'boyolali', 'jawa tengah', '57375', '-', '086754321345', 'ngemplak.fokus@gmail.com', '2016-09-13 14:14:24', '2016-09-13 14:14:24'),
(6, 8, 'Abu Jundi', 'Ngemplak', 'Boyolali', 'Jawa Tengah', '57375', '-', '081548213835', 'clara.haruna112@gmail.com', '2016-09-16 03:48:30', '2016-09-16 03:48:30'),
(7, 11, 'Sabilillah', 'Laweyan', 'Solo', 'Jawa Tengah', '57135', '-', '081548213835', 'sabilillahgroup@gmail.com', '2016-09-19 23:29:59', '2016-09-19 23:29:59'),
(8, 12, 'bebas widada', 'Jl Samanhudi No 2', 'Solo', 'Jawa Tengah', '57105', '-', '08123456789', 'bbswdd@gmail.com', '2016-09-20 04:59:44', '2016-09-20 04:59:44'),
(9, 15, 'hajiku', 'Welar', 'Surakarta', 'Jawa Tengah', '57145', '-', '084321566767', 'abu.usamah.alindunisy@gmail.com', '2016-10-21 01:30:53', '2016-10-21 01:30:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produks`
--

CREATE TABLE IF NOT EXISTS `produks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kategori_id` int(11) NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `hargabeli` int(11) NOT NULL,
  `gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dibaca` int(11) NOT NULL,
  `berat` int(5) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=54 ;

--
-- Dumping data untuk tabel `produks`
--

INSERT INTO `produks` (`id`, `kategori_id`, `judul`, `seo`, `deskripsi`, `stock`, `harga`, `hargabeli`, `gambar`, `dibaca`, `berat`, `created_at`, `updated_at`) VALUES
(4, 2, 'Longdress Kerah', 'longdress-kerah', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>L</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 10, 75000, 55000, '14715279326.jpg', 8, 300, '2016-06-11 15:47:07', '2016-10-27 00:27:47'),
(5, 2, 'Oblong Hitam', 'oblong-hitam', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>L</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 13, 40000, 20000, '14715278694.jpg', 8, 300, '2016-06-11 15:47:32', '2016-10-27 00:27:16'),
(6, 2, 'Longdress Ungu', 'longdress-ungu', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>L</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 14, 70000, 50000, '14715279005.jpg', 7, 300, '2016-06-11 16:09:06', '2016-10-27 00:26:29'),
(7, 2, 'Bunga Melati', 'bunga-melati', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>L</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 20, 40000, 20000, '14715278213.jpg', 25, 300, '2016-06-11 16:09:26', '2016-10-27 00:24:58'),
(8, 2, 'Oblong Ungu', 'oblong-ungu', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>L</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 16, 45000, 25000, '14715277882.jpg', 40, 300, '2016-06-11 16:09:42', '2016-10-27 00:24:14'),
(9, 2, 'Bunga-Bunga', 'bunga-bunga', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>L</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 14, 55000, 35000, '14715277631.jpg', 12, 300, '2016-06-11 16:09:59', '2016-10-27 00:23:15'),
(10, 2, 'Lumut Hijau', 'lumut-hijau', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>L</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 14, 65000, 45000, '14715889567.jpg', 1, 300, '2016-08-18 16:42:36', '2016-10-27 00:22:32'),
(11, 2, 'Obat Nyamuk', 'obat-nyamuk', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>L</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 15, 55000, 35000, '14715890058.jpg', 2, 300, '2016-08-18 16:43:25', '2016-10-27 00:21:44'),
(12, 2, 'Bunga Mawar', 'bunga-mawar', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>L</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 15, 50000, 30000, '14715890479.jpg', 2, 300, '2016-08-18 16:44:07', '2016-10-27 00:21:09'),
(13, 2, 'Kekelawar', 'kekelawar', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>L</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 15, 75000, 55000, '147158910710.jpg', 2, 300, '2016-08-18 16:45:07', '2016-10-27 00:20:17'),
(14, 2, 'Bunga Cemara', 'bunga-cemara', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Prima Halus</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 260</li>\r\n<li>Lebar: 99</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 13, 155000, 135000, '14715892871.jpg', 1, 700, '2016-08-18 16:48:07', '2016-10-27 00:18:48'),
(15, 2, 'Segitiga Besar', 'segitiga-besar', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Prima Halus</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 260</li>\r\n<li>Lebar: 99</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 12, 120000, 100000, '14715893222.jpg', 1, 700, '2016-08-18 16:48:42', '2016-10-27 00:18:06'),
(16, 2, 'Segitiga Kecil', 'segitiga-kecil', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Prima Halus</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 260</li>\r\n<li>Lebar: 99</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 12, 135000, 115000, '14715893543.jpg', 2, 700, '2016-08-18 16:49:14', '2016-10-27 00:17:36'),
(17, 2, 'Analisa', 'analisa', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Prima Halus</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 260</li>\r\n<li>Lebar: 99</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 13, 150000, 130000, '14715893904.jpg', 2, 700, '2016-08-18 16:49:50', '2016-10-27 00:17:02'),
(18, 2, 'Bunga Biru Hitam', 'bunga-biru-hitam', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Prima Halus</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 260</li>\r\n<li>Lebar: 99</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 13, 150000, 130000, '14715896905.jpg', 2, 700, '2016-08-18 16:54:50', '2016-10-27 00:16:26'),
(19, 2, 'Bunga Coklatl', 'bunga-coklatl', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Prima Halus</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 260</li>\r\n<li>Lebar: 99</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 11, 120000, 100000, '14715897496.jpg', 1, 700, '2016-08-18 16:55:49', '2016-10-27 00:15:43'),
(20, 2, 'Bunga Biru Campur', 'bunga-biru-campur', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Prima Halus</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 260</li>\r\n<li>Lebar: 99</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 15, 127000, 107000, '14715907577.jpg', 0, 700, '2016-08-18 17:12:37', '2016-10-27 00:15:14'),
(21, 2, 'Bunga Abu-Abu', 'bunga-abu-abu', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Prima Halus</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 260</li>\r\n<li>Lebar: 99</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 13, 125000, 105000, '14715909208.jpg', 0, 700, '2016-08-18 17:15:20', '2016-10-27 00:14:23'),
(22, 2, 'Bunga Pink', 'bunga-pink', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Prima Halus</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 260</li>\r\n<li>Lebar: 99</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 15, 138000, 118000, '14715909659.jpg', 0, 700, '2016-08-18 17:16:05', '2016-10-27 00:14:42'),
(23, 2, 'Bunga Pot', 'bunga-pot', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Prima Halus</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 260</li>\r\n<li>Lebar: 99</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 17, 130000, 110000, '147159109710.jpg', 0, 700, '2016-08-18 17:18:18', '2016-10-27 00:13:01'),
(24, 2, 'Tlenok Putih', 'tlenok-putih', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Syantung Halus Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 117</li>\r\n<li>Lebar: 68</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>Kimono dan Tali</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 10, 98000, 78000, '14715911581.jpg', 2, 500, '2016-08-18 17:19:18', '2016-10-27 00:11:11'),
(25, 2, 'Waru Biru Laut', 'waru-biru-laut', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Syantung Halus Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 117</li>\r\n<li>Lebar: 68</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>Kimono dan Tali</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 12, 80000, 60000, '14715911922.jpg', 4, 500, '2016-08-18 17:19:52', '2016-10-27 00:10:40'),
(26, 2, 'Lurik-Lurik', 'lurik-lurik', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Syantung Halus Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 117</li>\r\n<li>Lebar: 68</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>Kimono dan Tali</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 13, 88000, 68000, '14715912213.jpg', 1, 500, '2016-08-18 17:20:21', '2016-10-27 00:10:01'),
(27, 2, 'Batik Waru', 'batik-waru', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Syantung Halus Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 117</li>\r\n<li>Lebar: 68</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>Kimono dan Tali</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 12, 90000, 70000, '14715912624.jpg', 1, 500, '2016-08-18 17:21:02', '2016-10-27 00:09:20'),
(28, 2, 'Batik Coklat', 'batik-coklat', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Syantung Halus Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 117</li>\r\n<li>Lebar: 68</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>Kimono dan Tali</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 10, 89000, 69000, '14715913075.jpg', 4, 500, '2016-08-18 17:21:47', '2016-10-27 00:08:27'),
(29, 2, 'Batik Lurik', 'batik-lurik', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Syantung Halus Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 117</li>\r\n<li>Lebar: 68</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>Kimono dan Tali</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 11, 97000, 77000, '14715913326.jpg', 2, 500, '2016-08-18 17:22:12', '2016-10-27 00:07:40'),
(30, 2, 'Batik Bunga', 'batik-bunga', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Syantung Halus Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 117</li>\r\n<li>Lebar: 68</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>Kimono dan Tali</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 13, 89000, 69000, '14715913617.jpg', 2, 500, '2016-08-18 17:22:41', '2016-10-27 00:07:00'),
(31, 2, 'Daun Biru', 'daun-biru', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Syantung Halus Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 117</li>\r\n<li>Lebar: 68</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>Kimono dan Tali</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 10, 95000, 75000, '14715913938.jpg', 1, 500, '2016-08-18 17:23:13', '2016-10-27 00:05:57'),
(32, 2, 'Lurik', 'lurik', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Syantung Halus Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 117</li>\r\n<li>Lebar: 68</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>Kimono dan Tali</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 8, 99000, 79000, '14715914229.jpg', 1, 500, '2016-08-18 17:23:42', '2016-10-27 00:04:56'),
(33, 2, 'Cermai Biru', 'cermai-biru', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Syantung Halus Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 117</li>\r\n<li>Lebar: 68</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>Kimono dan Tali</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 11, 100000, 80000, '147159146910.jpg', 2, 500, '2016-08-18 17:24:29', '2016-10-27 00:04:01'),
(34, 2, 'Tlenok Biru', 'tlenok-biru', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Rayon Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 200</li>\r\n<li>Lebar: 180</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>1 sprei, 2 sarung bantal dan 2 sarung guling</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 12, 225000, 205000, '14715915561.jpg', 2, 1200, '2016-08-18 17:25:56', '2016-10-27 00:01:42'),
(35, 2, 'Bunga Tulip', 'bunga-tulip', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Rayon Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 200</li>\r\n<li>Lebar: 180</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>1 sprei, 2 sarung bantal dan 2 sarung guling</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 10, 200000, 180000, '14715916622.jpg', 2, 1200, '2016-08-18 17:27:42', '2016-10-27 00:00:08'),
(36, 2, 'Polos Batik', 'polos-batik', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Rayon Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 200</li>\r\n<li>Lebar: 180</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>1 sprei, 2 sarung bantal dan 2 sarung guling</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 11, 225000, 205000, '14715916963.jpg', 1, 1200, '2016-08-18 17:28:16', '2016-10-26 23:58:03'),
(37, 2, 'Bundar Merah Tua', 'bundar-merah-tua', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Rayon Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 200</li>\r\n<li>Lebar: 180</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>1 sprei, 2 sarung bantal dan 2 sarung guling</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 10, 200000, 180000, '14715917244.jpg', 0, 1200, '2016-08-18 17:28:44', '2016-10-26 23:53:57'),
(38, 2, 'Bunga Segiempat', 'bunga-segiempat', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Rayon Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 200</li>\r\n<li>Lebar: 180</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>1 sprei, 2 sarung bantal dan 2 sarung guling</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 11, 210000, 190000, '14715917505.jpg', 0, 1200, '2016-08-18 17:29:10', '2016-10-26 23:53:19'),
(39, 2, 'Rame Biru', 'rame-biru', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Rayon Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 200</li>\r\n<li>Lebar: 180</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>1 sprei, 2 sarung bantal dan 2 sarung guling</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 11, 200000, 180000, '14715917726.jpg', 0, 1200, '2016-08-18 17:29:32', '2016-10-26 23:52:53'),
(40, 2, 'Polos Segiempat', 'polos-segiempat', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Rayon Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 200</li>\r\n<li>Lebar: 180</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>1 sprei, 2 sarung bantal dan 2 sarung guling</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 25, 220000, 200000, '14715917987.jpg', 0, 1200, '2016-08-18 17:29:58', '2016-10-26 23:52:15'),
(41, 2, 'Akar Merah Tua', 'akar-merah-tua', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Rayon Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 200</li>\r\n<li>Lebar: 180</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>1 sprei, 2 sarung bantal dan 2 sarung guling</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 10, 210000, 190000, '14715918298.jpg', 0, 1200, '2016-08-18 17:30:29', '2016-10-26 23:51:24'),
(42, 2, 'Ceplok Merah', 'ceplok-merah', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Rayon Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 200</li>\r\n<li>Lebar: 180</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>1 sprei, 2 sarung bantal dan 2 sarung guling</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 12, 220000, 200000, '14715918609.jpg', 1, 1200, '2016-08-18 17:31:00', '2016-10-26 23:50:28'),
(43, 2, 'Daun Segiempat Ungu', 'daun-segiempat-ungu', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Rayon Super</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Panjang: 200</li>\r\n<li>Lebar: 180</li>\r\n</ul>\r\n<p><strong>Isi:</strong></p>\r\n<ul>\r\n<li>1 sprei, 2 sarung bantal dan 2 sarung guling</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 17, 225000, 205000, '147159189310.jpg', 1, 1200, '2016-08-18 17:31:33', '2016-10-26 23:50:50'),
(44, 2, 'Akar Putih', 'akar-putih', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Polino</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Atasan: Dari dagu ke bawah 77 cm, dari dahi ke belakang 123 cm</li>\r\n<li>Bawahan: Lebar 77 cm, panjang 117 cm&nbsp;</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 10, 200000, 180000, '14715920271.jpg', 12, 1000, '2016-08-18 17:33:47', '2016-10-26 01:41:47'),
(45, 2, 'Tlenok Hijau', 'tlenok-hijau', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Polino</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Atasan: Dari dagu ke bawah 77 cm, dari dahi ke belakang 123 cm</li>\r\n<li>Bawahan: Lebar 77 cm, panjang 117 cm&nbsp;</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 12, 180000, 160000, '14715920532.jpg', 14, 1000, '2016-08-18 17:34:13', '2016-10-26 01:41:27'),
(46, 2, 'Kembang', 'kembang', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Polino</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Atasan: Dari dagu ke bawah 77 cm, dari dahi ke belakang 123 cm</li>\r\n<li>Bawahan: Lebar 77 cm, panjang 117 cm&nbsp;</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 17, 200000, 180000, '14715920883.jpg', 14, 1000, '2016-08-18 17:34:48', '2016-10-26 01:43:29'),
(47, 2, 'Kembang Mawar', 'kembang-mawar', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Polino</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Atasan: Dari dagu ke bawah 77 cm, dari dahi ke belakang 123 cm</li>\r\n<li>Bawahan: Lebar 77 cm, panjang 117 cm&nbsp;</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 17, 190000, 170000, '14715921134.jpg', 20, 1000, '2016-08-18 17:35:14', '2016-10-26 01:43:16'),
(48, 2, 'Bunga Violet', 'bunga-violet', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Polino</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Atasan: Dari dagu ke bawah 77 cm, dari dahi ke belakang 123 cm</li>\r\n<li>Bawahan: Lebar 77 cm, panjang 117 cm&nbsp;</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 11, 190000, 170000, '14715921395.jpg', 15, 1000, '2016-08-18 17:35:39', '2016-10-26 01:43:04'),
(49, 2, 'Segitiga Hijau', 'segitiga-hijau', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Polino</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Atasan: Dari dagu ke bawah 77 cm, dari dahi ke belakang 123 cm</li>\r\n<li>Bawahan: Lebar 77 cm, panjang 117 cm&nbsp;</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 13, 200000, 180000, '14715921706.jpg', 15, 1000, '2016-08-18 17:36:10', '2016-10-26 01:42:51'),
(50, 2, 'Gelombang Biru', 'gelombang-biru', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Polino</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Atasan: Dari dagu ke bawah 77 cm, dari dahi ke belakang 123 cm</li>\r\n<li>Bawahan: Lebar 77 cm, panjang 117 cm&nbsp;</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 15, 200000, 180000, '14715922017.jpg', 17, 1000, '2016-08-18 17:36:41', '2016-10-26 01:42:41'),
(51, 2, 'Batik Ungu', 'batik-ungu', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Polino</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Atasan: Dari dagu ke bawah 77 cm, dari dahi ke belakang 123 cm</li>\r\n<li>Bawahan: Lebar 77 cm, panjang 117 cm&nbsp;</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 15, 180000, 160000, '14715922288.jpg', 21, 1000, '2016-08-18 17:37:09', '2016-10-26 01:42:30'),
(52, 2, 'Rumput Putih', 'rumput-putih', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Polino</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Atasan: Dari dagu ke bawah 77 cm, dari dahi ke belakang 123 cm</li>\r\n<li>Bawahan: Lebar 77 cm, panjang 117 cm&nbsp;</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 29, 200000, 180000, '14715922559.jpg', 47, 1000, '2016-08-18 17:37:36', '2016-10-26 01:42:19'),
(53, 2, 'Cinde Kuning', 'cinde-kuning', '<p><strong>Bahan:</strong></p>\r\n<ul>\r\n<li>Material Katun Polino</li>\r\n</ul>\r\n<p><strong>Ukuran:</strong></p>\r\n<ul>\r\n<li>Atasan: Dari dagu ke bawah 77 cm, dari dahi ke belakang 123 cm</li>\r\n<li>Bawahan: Lebar 77 cm, panjang 117 cm&nbsp;</li>\r\n</ul>\r\n<p><strong>Jenis Batik:</strong></p>\r\n<ul>\r\n<li>Sablon</li>\r\n</ul>', 21, 195000, 175000, '147159229010.jpg', 33, 1000, '2016-08-18 17:38:10', '2016-10-26 01:42:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `returs`
--

CREATE TABLE IF NOT EXISTS `returs` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `nohp` varchar(15) NOT NULL,
  `namabarang` varchar(100) NOT NULL,
  `alasan` varchar(255) NOT NULL,
  `pilihan` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `slideshows`
--

CREATE TABLE IF NOT EXISTS `slideshows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `slideshows`
--

INSERT INTO `slideshows` (`id`, `judul`, `gambar`, `link`, `created_at`, `updated_at`) VALUES
(1, 'Slide1', '1471527579slide4.jpg', '', '2016-06-13 22:23:34', '2016-08-17 23:39:40'),
(2, 'Slide2', '1471527567slide3.jpg', '', '2016-06-13 22:23:48', '2016-08-17 23:39:28'),
(3, 'Slide3', '1471527550slide2.jpg', '', '2016-06-13 22:23:58', '2016-08-17 23:39:10'),
(4, 'Slide4', '147395664911755268_1150028265014165_6050994873918348808_n (1).jpg', '', '2016-06-13 22:24:09', '2016-09-15 16:24:10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statis`
--

CREATE TABLE IF NOT EXISTS `statis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isi` text COLLATE utf8_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dibaca` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `statis`
--

INSERT INTO `statis` (`id`, `judul`, `seo`, `isi`, `gambar`, `dibaca`, `created_at`, `updated_at`) VALUES
(1, 'SUPPORT ONLINE ', 'support-online', '<p><strong>SENIN - SABTU |&nbsp;8.00 - 16.00 WIB</strong></p>\r\n', '1465299102laravel_twitter.jpg', 0, '2016-06-06 21:31:19', '2016-10-06 20:12:11'),
(2, 'PENGIRIMAN TERPERCAYA', 'pengiriman-terpercaya', '<p><strong>PENGIRIMAN BARANG MENGGUNAKAN&nbsp;JNE</strong></p>\r\n', '1465712144laravel_twitter.jpg', 0, '2016-06-11 16:15:44', '2016-09-14 16:16:17'),
(3, 'BELANJA MENYENANGKAN', 'belanja-menyenangkan', '<p><strong>BISA DITUKAR JIKA TAK SESUAI HARAPAN</strong></p>\r\n', '1465742221duniaanak2.png', 0, '2016-06-12 00:37:02', '2016-08-26 07:33:04'),
(4, 'Profil Butik Anis', 'profil-butik-anis', '<p>Butik Anis merupakan toko yang menjual produk bermotif batik seperti pakaian daster, sprei dan jarik di Kota Boyolali. Butik ini didirikan pada tahun 2012 oleh Ibu Anis Tri Hatmini dan beralamat di dusun Garen RT 03 RW 03 desa Pandeyan kecamatan Ngemplak kabupaten Boyolali.</p>\r\n\r\n<p>Butik ini berawal dari hal coba-coba membeli sebuah pakaian daster bermotif batik di sebuah pasar Kota Solo, kemudian memfoto barang tersebut lalu meng-<em>upload</em>-nya di media sosial BBM dan forum jual beli di <em>Facebook</em>. Kemudian beberapa hari setelah itu, ada teman yang suka dan menanyakan barang tersebut kemudian memesan barang tersebut beberapa buah. Semenjak kejadian itu, mulai banyak pesanan dari beberapa teman dan orang lain. Kemudian suatu hari ada beberapa teman yang menanyakan barang yang lain yaitu sprei. Keesokan hari mencoba mencari barang tersebut di sebuah pasar Kota Solo kemudian memfoto barang yang diminta teman beberapa buah, setelah itu meng-<em>upload</em>-nya di media social dan mereka ternyata tertarik lalu memesan beberapa buah sprei itu. Kemudian di hari yang lain selain berbelanja pesanan, juga mencoba untuk memfoto barang lain yaitu jarik kemudian meng-<em>upload</em>-nya di BBM dan forum jual beli di <em>Facebook</em>. Tidak disangka setelah meng-<em>upload</em> jarik di forum jual beli di <em>Facebook</em>, ada sebuah tempat usaha sepatu dan tas di Kota Jakarta yang memesan jarik dalam jumlah besar yang dijadikan bahan untuk melapisi sepatu dan tas pada usaha miliknya. Berjalannya waktu dari hari ke hari usaha kecil ini semakin berkembang, mulai dari lingkungan keluarga besar kemudian lingkungan tetangga sekitar kemudian dari dalam kota maupun luar kota dan bahkan sampai dari Papua pun pernah ada yang memesan salah satu barang.</p>\r\n', '14739579741656281_803537296329932_1048359195_n.jpg', 0, '2016-08-24 19:57:30', '2016-09-15 16:46:15'),
(5, 'Cara Belanja', 'cara-belanja', '<ul>\r\n<li><strong>Pilih Barang lalu Klik Beli</strong></li>\r\n</ul>\r\n<p><img src="../../../gambar/1_1.JPG" alt="1_1" /></p>\r\n<ul>\r\n<li><strong>Kemudian Klik Proses Pembayaran</strong></li>\r\n</ul>\r\n<p><strong><img src="../../../gambar/2.JPG" alt="2" /></strong></p>\r\n<ul>\r\n<li><strong>Isi Data (Pelanggan Baru/ Pelanggan Tetap)</strong></li>\r\n</ul>\r\n<p><strong><img src="../../../gambar/3.JPG" alt="3" /></strong></p>\r\n<ul>\r\n<li><strong>Klik Ubah Data (Pelanggan Baru)</strong></li>\r\n</ul>\r\n<p><strong><img src="../../../gambar/4.JPG" alt="4" /></strong></p>\r\n<ul>\r\n<li><strong>Isi atau Ubah Data</strong></li>\r\n</ul>\r\n<p><strong><img src="../../../gambar/5.JPG" alt="5" /></strong></p>\r\n<ul>\r\n<li><strong>Klik Lanjut Pembayaran</strong></li>\r\n</ul>\r\n<p><strong><img src="../../../gambar/6.JPG" alt="6" /></strong></p>\r\n<ul>\r\n<li><strong>Klik Simpan Pesanan</strong></li>\r\n</ul>\r\n<p><img src="../../../gambar/7.JPG" alt="7" /></p>\r\n<ul>\r\n<li><strong>Tampil Pesan untuk Menunggu Balasan dari Butik Anis</strong></li>\r\n</ul>\r\n<p><strong><img src="../../../gambar/8.JPG" alt="8" /></strong></p>\r\n<p style="text-align: center;"><a href="../../../biaya-ongkir">DAFTAR ONGKIR</a></p>', '1473958174shopping-cart1.jpg', 0, '2016-08-24 19:57:47', '2016-10-27 01:21:07'),
(6, 'Selamat Datang di Butik Anis', 'selamat-datang-di-butik-anis', '<p><strong>Selamat datang di Butik Anis Boyolali!</strong></p>\r\n\r\n<p>Butik Anis menyediakan berbagai macam produk batik yang berkualitas dengan harga terjangkau.</p>\r\n\r\n<p>Kami menyediakan&nbsp;produk batik seperti daster, sprei, kimono, mukena dan jarik.</p>\r\n\r\n<p>Bahan kain&nbsp;yang kami gunakan sebagian besar terbuat dari kain katun.</p>\r\n\r\n<p><em>Selamat berbelanja di Butik Anis Boyolali!</em></p>\r\n', '1473958907fPro-Butik-Galeri-Batik-Jawa1.jpg', 0, '2016-08-29 20:55:01', '2016-09-15 17:04:39'),
(7, 'Ketentuan Retur', 'ketentuan-retur', '<h3><strong>SYARAT DAN KETENTUAN PENGEMBALIAN</strong></h3>\r\n<ol>\r\n<li>Anda dapat melakukan pengembalian barang dalam jangka waktu 3&nbsp;hari (termasuk hari libur) terhitung sejak barang Anda terima. Saat Anda menerima barang sudah terhitung sebagai 1 hari.</li>\r\n<li>Produk harus dikirimkan dalam kondisi asli dan berada dalam kemasan lengkap dengan aksesoris terkait.</li>\r\n<li>Produk tidak dalam keadaan rusak, kotor, telah dipakai, dan&nbsp;tercelup/terkena air.</li>\r\n<li>Harap tidak mengisolasi barang secara berlebihan tetapi cukup membungkusnya untuk mencegah kerusakan. Barang tersebut tetap menjadi tanggung jawab Anda sampai Butik Anis menerimanya.</li>\r\n<li>Mohon bantuan Anda untuk mengembalikan paket dengan hati-hati.</li>\r\n<li>Pihak Butik Anis&nbsp;akan melakukan pengecekan kembali akan kualitas produk yang dikembalikan. Apabila&nbsp;ada persyaratan yang tidak dipenuhi, Butik Anis berhak menolak pengembalian produk tersebut.</li>\r\n<li>Untuk pengembalian barang hanya dapat dilakukan dengan memilih salah satu metode pengembalian :\r\n<ul>\r\n<li>Penukaran barang yang sama (warna/ukuran)</li>\r\n<li>Pengembalian dana (Refund rekening/kredit)</li>\r\n</ul>\r\n</li>\r\n<li>Mengisi formulir pengembalian barang dengan lengkap dan benar sesuai dengan petunjuk pengisian.</li>\r\n<li>Retur/ refund Anda akan selesai kami proses 3-4 hari kerja semenjak barang retur diterima di warehouse kami</li>\r\n<li>Butik Anis akan mengganti biaya pengembalian barang apabila kesalahan ada di pihak kami. Selain alasan itu biaya pengembalian barang ditanggung oleh pembeli</li>\r\n<li>Biaya pengembalian barang akan diproses dalam waktu 3&nbsp;x 24 jam&nbsp;berlaku apabila&nbsp;kesalahan ada di pihak kami</li>\r\n<li>Anda diwajibkan mengirimkan foto bukti resi&nbsp;ke&nbsp;butik.anis@gmail.com atau ke nomor whatsapp Butik Anis sebelum barang retur diterima di warehouse&nbsp;Butik Anis berlaku apabila&nbsp;kesalahan ada di pihak kami</li>\r\n</ol>\r\n<p style="text-align: center;"><a href="http://butikanis.com/confirm/create">FORMULIR RETUR</a></p>', '1474351674Info Retur.jpg', 0, '2016-09-16 02:29:42', '2016-10-26 01:20:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` enum('user','admin') COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'administrator@gmail.com', '$2y$10$1mbLjZNa8raRE4j0MXBl2.lAYFbdSmVeQKvDTGbZNlPKXbo2wqbMy', 'admin', 'VT0ixES2IeXJ6tkH90nwjP8J5VbDhDA8f4srH10xqA9U13o0UEpe3YgFtpk3', '2016-08-15 18:56:30', '2016-11-07 11:23:54'),
(2, 'M. YUsron Adnan', 'marsonosaputro@gmail.com', '$2y$10$Z7E7sypsRjbsRLVYLqHZx.yHPiB6.5oUMOH2aqUqXVw72BAlKRSPu', 'user', '6Hg8mTg9T0r2JCjuzc6IIfEIsUuFyNM4DMaLLJz1tZAo7vDp1z55k3BGcO6P', '2016-08-16 12:16:39', '2016-09-04 00:23:28'),
(12, 'bebas widada', 'bbswdd@gmail.com', '$2y$10$qJtYdHQyeS4kO85XEiAlcuQZQrGTLs7dGY31zM8sFsGSlBW77NwFC', 'user', 'BJKvnDYfdckJFbamF8BxLW7VbdkpjY1YHjNhDT88qOFfqKXiQCSbMNTBdi95', '2016-09-20 04:58:20', '2016-09-20 05:25:05'),
(11, 'Sabilillah', 'sabilillahgroup@gmail.com', '$2y$10$2qmtzghymn9BtOlrmg3caOxZ6GNq2QWliu8UdBq9NutkGe1d7TcRW', 'user', 'gRDwiQp8v4u6Os1BB3wuWVCJu6eClPgXvTC7xK3Lj3pHS9xRHdMPw6PnF32w', '2016-09-19 23:28:31', '2016-09-20 02:13:19'),
(10, 'Bambang', 'kuliahbsn09@gmail.com', '$2y$10$l6hWAXPYpEoQ.CZ7XJmmcehfXim14aw0/vF2w7A5Q1jF/v97OviNy', 'user', NULL, '2016-09-16 03:44:30', '2016-09-16 03:44:30'),
(9, 'Yunus Nova', 'clara.haruna112@gmail.com', '$2y$10$.jpxIGZB0OC2Wsb0iZ1b.uhaKqAoTWvmEGILfYrADvUkkm0TTBi66', 'user', '2nCxr5hnXkT3loJYHeUVlBvD0EoZX0tcqyFiD1NLqlekTMR66oIa7EZiaoVB', '2016-09-16 02:46:43', '2016-09-16 02:58:42'),
(8, 'Yunus', 'yunusnova.wb@gmail.com', '$2y$10$w/9EIlEHJr0gD7oDhRhLwOi1eAcWfQpG2.h4RbiM9BVCt.dvdvph6', 'user', NULL, '2016-09-16 02:43:19', '2016-09-16 02:43:19'),
(13, 'yunuz', 'yunus.novawirawan@gmail.com', '$2y$10$Yz0IegMlZ5mtsI7NmL5FheN5d/5UU.bRN3QNJaOeKgmcBnu58SsAy', 'user', 'GaODYHWm9hl3yYfxUVJNtnrCmM5Qc7twK03PlGOecwbUtcb9oJVGEr0EuUDb', '2016-10-20 18:38:27', '2016-10-20 18:39:38'),
(14, 'haji', 'yunusnova.mas@gmail.com', '$2y$10$S4LqXdA.n5R/8w2QGn82h.RdRmkgxAqoHF/M2ie1tMZR0XyNsz2vW', 'user', 'XIYbFpSKpYTxV7PsNiW4MdcxTyU9TklkYBlkf9rfJU59V3IntqC1QzrcIjV8', '2016-10-20 18:41:17', '2016-10-20 18:44:42'),
(15, 'hajiku', 'abu.usamah.alindunisy@gmail.com', '$2y$10$1qYbg.o3hL0XkHb2jqEA5O0/OJCS75bT6eSTSsVyOabNbf2QmMTQC', 'user', 'AZUSIZlD7i8ya865tQLvhULMyBUEub9Hv0lDAMvl24TqXLQ3ceKtXg5LDtxN', '2016-10-20 19:15:06', '2016-10-21 01:46:16');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
