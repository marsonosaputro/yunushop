@extends('frontend')
@section('content')
    <div id="content">
    <div class="container">
    <div class="col-sm-12">
    <ul class="breadcrumb">
    <li><a href="{{ url('/') }}">Home</a>
    </li>
    <li>Artikel</li>
    </ul>
    </div>
            <!-- *** LEFT COLUMN ***
         _________________________________________________________ -->
    <div class="col-sm-9" id="blog-listing">
@foreach($artikel as $key => $d)
    <div class="post">
    <h2><a href="{{ url('blog/'.$d->seo) }}">{{ $d->judul }}</a></h2>
    <p class="author-category">Oleh: <a href="#">{{ $d->penulis }}</a> Kategori: <a href="">{{ baca_kategori_artikel($d->kategori_artikel_id) }}</a>
    </p>
    <hr>
    <p class="date-comments">
    <a href="#"><i class="fa fa-calendar-o"></i> {{ $d->created_at }}</a>
                            <!--<a href="#"><i class="fa fa-comment-o"></i> 8 Comments</a>-->
    </p>
    <div class="image">
    <a href="#">
    <img src="{{ asset('gambar/artikel/'.$d->gambar) }}" class="img-responsive" alt="YunusSHOP">
    </a>
    </div>
    <p class="intro">{!! str_limit($d->artikel, 350) !!}</p>
    <p class="read-more"><a href="{{ url('blog/'.$d->seo) }}" class="btn btn-primary">Selengkapnya...</a>
    </p>
    </div>
@endforeach
    <div class="pages">
    {!! $artikel->render() !!}
    </div>
    </div>
            <!-- /.col-md-9 -->
            <!-- *** LEFT COLUMN END *** -->
    <div class="col-md-3">
                <!-- *** BLOG MENU ***
_________________________________________________________ -->
    <div class="panel panel-default sidebar-menu">
    <div class="panel-heading">
    <h3 class="panel-title">Kategori Artikel</h3>
    </div>
    <div class="panel-body">
    <ul class="nav nav-pills nav-stacked">
@foreach($kategori as $key => $d)
    <li><a href="{{ url('blog/kategori/'.$d->id) }}">{{ $d->judul }}</a></li>
@endforeach
    </ul>
    </div>
    </div>
                <!-- /.col-md-3 -->
                <!-- *** BLOG MENU END *** -->
    <div class="box">
    <div class="box-header">
    <h4>Rekening Pembayaran</h4>
    </div>
    <img src="{{ asset('frontend/') }}/img/bank 1.jpg" alt="sales 2014" class="img-responsive"><br>
    <img src="{{ asset('frontend/') }}/img/bank 2.jpg" alt="sales 2014" class="img-responsive">
    </div>
    <div class="box">
    <div class="box-header">
    <h4>Jasa Pengiriman Barang</h4>
    </div>
    <img src="{{ asset('frontend/') }}/img/JNE.jpg" alt="sales 2014" class="img-responsive">
    </div>
    </div>
    </div>
        <!-- /.container -->
    </div>
@endsection
