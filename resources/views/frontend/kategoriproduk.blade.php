@extends('frontend')
@section('content')
    <div id="all">

        <div id="content">
            <div class="container">

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a>
                        </li>
                        <li>Produk</li>
                    </ul>
                </div>

                <div class="col-md-3">
                    @include('frontend.sidebar')
                </div>

                <div class="col-md-9">
                    <div class="row products">
                        @foreach ($produk as $d)
                            <div class="col-md-4 col-sm-6">
                                <div class="product">
                                    <div class="flip-container">
                                        <div class="flipper">
                                            <div class="front">
                                                <a href="{{ url('detail-produk/'.$d->seo) }}">
                                                    <img src="{{ asset('gambar/produk/'.$d->gambar) }}" alt="" class="img-responsive">
                                                </a>
                                            </div>
                                            <div class="back">
                                                <a href="{{ url('detail-produk/'.$d->seo) }}">
                                                    <img src="{{ asset('gambar/produk/'.$d->gambar) }}" alt="" class="img-responsive">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{ url('detail-produk/'.$d->seo) }}">
                                        <img src="{{ asset('gambar/produk/'.$d->gambar) }}" alt="" class="img-responsive">
                                    </a>
                                    <div class="text">
                                        <h3><a href="{{ url('detail-produk/'.$d->seo) }}">{{ $d->judul }}</a></h3>
                                        <p class="price">Rp. {{ number_format($d->harga,0,'.',',') }}</p>
                                        <p class="buttons">
                                            <a href="{{ url('detail-produk/'.$d->seo) }}" class="btn btn-default">Detail</a>
                                            <a href="{{ url('product/cart/'.$d->id) }}" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>Beli</a>
                                        </p>
                                    </div>
                                    <!-- /.text -->
                                </div>
                                <!-- /.product -->
                            </div>
                        @endforeach

                        <!-- /.col-md-4 -->
                    </div>
                    <!-- /.products -->

                    <div class="pages">
                        {!! $produk->render() !!}
                    </div>


                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
@endsection
