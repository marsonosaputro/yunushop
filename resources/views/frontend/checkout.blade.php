@extends('frontend')
@section('content')
    <div id="all">
    <div id="content">
    <div class="container">
    <div class="col-md-12">
    <ul class="breadcrumb">
    <li><a href="#">Home</a>
    </li>
    <li>Sign up/ Sign in</li>
    </ul>
    </div>
    <div class="col-md-6">
    <div class="box">
    <h1>Pelanggan Baru</h1>
    <p class="lead">Anda belum terdaftar jadi pelanggan kami?</p>
    <p class="text-muted">Silakan daftar terlebih dahulu!</p>
    <hr>
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
    {{ csrf_field() }}
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="name" class="col-md-3 control-label">Nama Lengkap</label>
    <div class="col-md-8">
    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
@if ($errors->has('name'))
    <span class="help-block">
    <strong>{{ $errors->first('name') }}</strong>
    </span>
@endif
    </div>
    </div>
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="email" class="col-md-3 control-label">E-mail</label>
    <div class="col-md-8">
    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
    @if ($errors->has('email'))
    <span class="help-block">
    <strong>{{ $errors->first('email') }}</strong>
    </span>
@endif
    </div>
    </div>
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    <label for="password" class="col-md-3 control-label">Password</label>
    <div class="col-md-8">
    <input id="password" type="password" class="form-control" name="password">
@if ($errors->has('password'))
    <span class="help-block">
    <strong>{{ $errors->first('password') }}</strong>
    </span>
@endif
    </div>
    </div>
    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
    <label for="password-confirm" class="col-md-3 control-label">Confirm Password</label>
    <div class="col-md-8">
    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
@if ($errors->has('password_confirmation'))
    <span class="help-block">
    <strong>{{ $errors->first('password_confirmation') }}</strong>
    </span>
@endif
    </div>
    </div>
    <div class="form-group">
    <div class="col-md-8 col-md-offset-3">
    <button type="submit" class="btn btn-primary">
    <i class="fa fa-btn fa-user"></i> Daftar
    </button>
    </div>
    </div>
    </form>
    </div>
    </div>
    <div class="col-md-6">
    <div class="box">
    <h1>Pelanggan Tetap</h1>
    <p class="lead">Anda pelanggan tetap kami?</p>
    <p class="text-muted">Silakan masuk dengan e-mail dan password Anda!</p>
    <hr>
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
    {{ csrf_field() }}
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="email" class="col-md-4 control-label">E-mail </label>
    <div class="col-md-6">
    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
@if ($errors->has('email'))
    <span class="help-block">
    <strong>{{ $errors->first('email') }}</strong>
    </span>
@endif
    </div>
    </div>
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    <label for="password" class="col-md-4 control-label">Password</label>
    <div class="col-md-6">
    <input id="password" type="password" class="form-control" name="password">
@if ($errors->has('password'))
    <span class="help-block">
    <strong>{{ $errors->first('password') }}</strong>
    </span>
@endif
    </div>
    </div>
    <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
    <div class="checkbox">
    <label>
    <input type="checkbox" name="remember"> Remember Me
    </label>
    </div>
    </div>
    </div>
    <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
    <button type="submit" class="btn btn-primary">
    <i class="fa fa-btn fa-sign-in"></i> Login
    </button>

    </div>
    </div>
    </form>
    </div>
    </div>
    </div>
            <!-- /.container -->
    </div>
        <!-- /#content -->
@endsection
