@extends('frontend')
@section('content')
    <div id="all">

        <div id="content">

            <div class="container">
                <div class="col-md-12">
                    <div id="main-slider">
                        @foreach($slideshow as $key => $d)
                            <div class="item">
                                <img class="img-responsive" src="{{ asset('gambar/slideshow/'.$d->gambar) }}" alt="">
                            </div>
                        @endforeach
                    </div>
                    <!-- /#main-slider -->
                </div>
            </div>

            <!-- *** ADVANTAGES HOMEPAGE ***
    _________________________________________________________ -->
            <div id="advantages">

                <div class="container">
                    <div class="same-height-row">
                        <div class="col-sm-4">
                            <div class="box same-height clickable">
                                <div class="icon"><i class="fa fa-university"></i>
                                </div>

                                <h3><a href="#"> {{ $statis1->judul }}</a></h3>
                                 {!! $statis1->isi !!}
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="box same-height clickable">
                                <div class="icon"><i class="fa fa-truck"></i>
                                </div>

                                <h3><a href="#">{{ $statis2->judul }}</a></h3>
                                 {!!$statis2->isi !!}
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="box same-height clickable">
                                <div class="icon"><i class="fa fa-refresh"></i>
                                </div>

                                <h3><a href="#">{{ $statis3->judul }}</a></h3>
                                 {!! $statis3->isi !!}
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->

                </div>
                <!-- /.container -->

            </div>
            <!-- /#advantages -->

            <!-- *** ADVANTAGES END *** -->

            <!-- *** HOT PRODUCT SLIDESHOW ***
    _________________________________________________________ -->
            <div id="hot">

                <div class="box">
                    <div class="container">
                        <div class="col-md-12">
                            <h2>Produk Terbaru Kami</h2>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="product-slider">
                        @foreach($produk as $key => $d)
                        <div class="item">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="{{ url('detail-produk/'.$d->seo) }}">
                                                <img src="{{ asset('gambar/produk/'.$d->gambar) }}" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="{{ url('detail-produk/'.$d->seo) }}">
                                                <img src="{{ asset('gambar/produk/'.$d->gambar) }}" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{ url('detail-produk/'.$d->seo) }}" class="invisible">
                                    <img src="{{ asset('gambar/produk/'.$d->gambar) }}" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="{{ url('detail-produk/'.$d->seo) }}">{{ $d->judul }}</a></h3>
                                    <p class="price">Rp {{ number_format($d->harga,0,'.',',') }}</p>
                                </div>
                                <!-- /.text -->

                            </div>
                            <!-- /.product -->
                        </div>
                        <!-- /.col-md-4 -->
                        @endforeach

                    </div>
                    <!-- /.product-slider -->
                </div>
                <!-- /.container -->

            </div>
            <!-- /#hot -->

            <!-- *** HOT END *** -->

            <!-- *** BLOG HOMEPAGE ***
    _________________________________________________________ -->

            <div class="box text-center" data-animate="fadeInUp">
                <div class="container">
                    <div class="col-md-12">
                        <h3 class="text-uppercase">Yang baru di artikel</h3>

                        <p class="lead">Artikel apa yang baru di Butik Anis? <a href="{{ url('blog') }}">Cek di sini!</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="container">

                <div class="col-md-12" data-animate="fadeInUp">

                    <div id="blog-homepage" class="row">
                        @foreach($artikel as $key => $d)
                            <div class="col-sm-6">
                                <div class="post">
                                    <h4><a href="#">{{ $d->judul }}</a></h4>
                                    <p class="author-category">Oleh: <a href="#">{{ $d->penulis }}</a> kategori: <a href="">{{ baca_kategori_artikel($d->kategori_artikel_id) }}</a>
                                    </p>
                                    <hr>
                                    <p class="intro">{!! str_limit($d->artikel, 250) !!}</p>
                                    <p class="read-more"><a href="{{ url('blog/'.$d->seo) }}" class="btn btn-primary">Selengkapnya</a>
                                    </p>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    <!-- /#blog-homepage -->
                </div>
            </div>
            <!-- /.container -->

            <!-- *** BLOG HOMEPAGE END *** -->


        </div>
        <!-- /#content -->

@endsection
