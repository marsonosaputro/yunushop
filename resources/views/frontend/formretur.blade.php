@extends('frontend')
@section('content')
    <div id="content">
    <div class="container">
    <div class="col-sm-12">
    <ul class="breadcrumb">
    <li><a href="{{ url('/') }}">Home</a>
    </li>
    <li>Konfirmasi Pembayaran</li>
    </ul>
    </div>
            <!-- *** LEFT COLUMN ***
         _________________________________________________________ -->
    <div class="col-sm-9" id="blog-listing">
    <div class="post">

      <h3>Konfirmasi Pembayaran</h3>
      <p>
        Isi lengkap Formulir berikut untuk konfirmasi pembayaran!
      </p>
      @if (session()->has('flash_notification.message'))
          <div class="alert alert-{{ session()->get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! session()->get('flash_notification.message') !!}
          </div>
      @endif

      {!! Form::open(['method' => 'POST', 'route'=>'retur.store', 'class' => 'form-horizontal']) !!}

          <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
              {!! Form::label('nama', 'Nama Lengkap', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('nama', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('nama') }}</small>
              </div>
          </div>
          <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
              {!! Form::label('alamat', 'Alamat', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('alamat', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('alamat') }}</small>
              </div>
          </div>
          <div class="form-group{{ $errors->has('nohp') ? ' has-error' : '' }}">
              {!! Form::label('nohp', 'Nomor HP', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('nohp', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('nohp') }}</small>
              </div>
          </div>
          <div class="form-group{{ $errors->has('bank') ? ' has-error' : '' }}">
              {!! Form::label('bank', 'Bank yang di pakai', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('bank', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('bank') }}</small>
              </div>
          </div>
          <div class="form-group{{ $errors->has('rekening') ? ' has-error' : '' }}">
              {!! Form::label('rekening', 'Nomor Rekening', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('rekening', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('rekening') }}</small>
              </div>
          </div>
          <div class="form-group{{ $errors->has('atasnama') ? ' has-error' : '' }}">
              {!! Form::label('atasnama', 'Atas Nama', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('atasnama', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('atasnama') }}</small>
              </div>
          </div>
          <div class="form-group{{ $errors->has('nominal') ? ' has-error' : '' }}">
              {!! Form::label('nominal', 'Nominal Transfer', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('nominal', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('nominal') }}</small>
              </div>
          </div>

          <div class="btn-group pull-right">
              {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
              {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>
      {!! Form::close() !!}

    <br>
    <hr>
    <p>
    <h4>Bagikan ke:</h4>
                    <!-- AddToAny BEGIN -->
    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
    <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
    <a class="a2a_button_facebook"></a>
    <a class="a2a_button_twitter"></a>
    <a class="a2a_button_google_plus"></a>
    <a class="a2a_button_whatsapp"></a>
    <a class="a2a_button_line"></a>
    <a class="a2a_button_sms"></a>
    </div>
    <script async src="https://static.addtoany.com/menu/page.js"></script>
                    <!-- AddToAny END -->
    </p>
    </div>
    </div>
            <!-- /.col-md-9 -->

            <!-- *** LEFT COLUMN END *** -->
    <div class="col-md-3">
@include('frontend.statisSidebar')
    </div>
    </div>
        <!-- /.container -->
    </div>
@endsection
