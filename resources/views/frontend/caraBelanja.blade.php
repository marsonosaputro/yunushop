@extends('frontend')
@section('content')
    <div id="content">
    <div class="container">
    <div class="col-sm-12">
    <ul class="breadcrumb">
    <li><a href="{{ url('/') }}">Home</a>
    </li>
    <li>Cara Belanja</li>
    </ul>
    </div>
            <!-- *** LEFT COLUMN ***
         _________________________________________________________ -->
    <div class="col-sm-9" id="blog-listing">
    <div class="post">
    <h2>{{ $statis->judul }}</h2>
    <p class="author-date">Oleh Admin</a> | {{ $statis->created_at }}</p>
    <img src="{{ asset('gambar/statis/'.$statis->gambar) }}" class="img-responsive">
    <hr>
    <p>{!! $statis->isi !!}</p>
    <br>
    <hr>
    <h4>Bagikan ke:</h4>
                    <!-- AddToAny BEGIN -->
    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
    <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
    <a class="a2a_button_facebook"></a>
    <a class="a2a_button_twitter"></a>
    <a class="a2a_button_google_plus"></a>
    <a class="a2a_button_whatsapp"></a>
    <a class="a2a_button_line"></a>
    <a class="a2a_button_sms"></a>
    </div>
    <script async src="https://static.addtoany.com/menu/page.js"></script>
                    <!-- AddToAny END -->
    </p>
    </div>
    </div>
            <!-- /.col-md-9 -->
            <!-- *** LEFT COLUMN END *** -->
    <div class="col-md-3">
@include('frontend.statisSidebar')
    </div>
    </div>
        <!-- /.container -->
    </div>
@endsection
