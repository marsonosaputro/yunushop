@extends('frontend')
@section('content')
    <div id="content">
    <div class="container">
    <div class="col-md-12">
    <ul class="breadcrumb">
    <li><a href="#">Home</a>
    </li>
    <li>Kontak</li>
    </ul>
    </div>
    <div class="col-md-3">
@include('frontend.statisSidebar')
    </div>
    <div class="col-md-9">
    <div class="box" id="contact">
    <h1>Kontak</h1>
    <p class="lead">Jika Anda bingung cara belanja atau mengalami masalah tentang produk kami?</p>
    <p>Silakan hubungi kami! Senin-Sabtu | 8.00-16.00 WIB.</p>
    <hr>
    <div class="row">
    <div class="col-sm-4">
    <h4><i class="fa fa-map-marker"></i> Alamat</h4>
    <strong><p>Garen RT 02/ IV
    <br>Pandeyan
    <br>Ngemplak
    <br>Boyolali 57375
    <br>Jawa Tengah</strong>
    </p>
    </div>
                        <!-- /.col-sm-4 -->
    <div class="col-sm-4">
    <h4><i class="fa fa-phone-square"></i> Telephone</h4>
    <p><strong>(0271)720611</strong>
    </p>
    <h4><i class="fa fa-tablet"></i> SMS</h4>
    <p><strong>08562689459</strong>
    </p>
    <h4><i class="fa fa-whatsapp"></i> Whatsapp</h4>
    <p><strong>081548213835</strong>
    </p>
    </div>
                        <!-- /.col-sm-4 -->
    <div class="col-sm-4">
    <h4><i class="fa fa-envelope"></i> E-mail</h4>
    <p><strong>butik.anis@gmail.com</strong>
    </p>
    </div>
                        <!-- /.col-sm-4 -->
    </div>
                    <!-- /.row -->

    <hr>
                <!--
                    <div id="map">

                    </div>

                    <hr>

                    <h2>Contact form</h2>

                    <form>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="firstname">Firstname</label>
                                    <input type="text" class="form-control" id="firstname">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="lastname">Lastname</label>
                                    <input type="text" class="form-control" id="lastname">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="email">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="subject">Subject</label>
                                    <input type="text" class="form-control" id="subject">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="message">Message</label>
                                    <textarea id="message" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send message</button>

                            </div>
                        </div>

                    </form>
                -->

    </div>


    </div>
            <!-- /.col-md-9 -->
    </div>
        <!-- /.container -->
    </div>
    <!-- /#content -->
@endsection
