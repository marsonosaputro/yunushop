<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/bower_components/admin-lte/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
<span class="input-group-btn">
  <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
</span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header"><b>MENU</b></li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="{{ url('admin/pesanan') }}"><span>Pesanan</span></a></li>
            <li><a href="{{ url('admin/laporan') }}"><span>Laporan</span></a></li>
            <li class="treeview">
                <a href="#"><span>Produk</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/admin/produk') }}"><span>Produk</span></a></li>
                    <li><a href="{{ url('/admin/kategori') }}"><span>Kategori</span></a></li>
                </ul>
            </li>
            <li><a href="{{ url('konfirmasi') }}"><span>Konfirmasi Transfer</span></a></li>
            <li><a href="{{ url('confirm') }}"><span>Retur</span></a></li>
            <li><a href="{{ url('/admin/slideshow') }}"><span>Slideshow</span></a></li>
            <li><a href="{{ url('/admin/statis') }}"><span>Halaman Statis</span></a></li>
            <li><a href="{{ url('/admin/ongkir') }}"><span>Ongkir</span></a></li>
            <li class="treeview">
                <a href="#"><span>Artikel</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/admin/artikel') }}">Data Artikel</a></li>
                    <li><a href="{{ url('/admin/kategoriartikel') }}">Kategori Artikel</a></li>
                </ul>
            </li>
            <li><a href="{{ url('admin/user') }}"><span>Manajemen User</span></a></li>
            <li><a href="{{ url('/logout') }}"><span>Logout</span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
