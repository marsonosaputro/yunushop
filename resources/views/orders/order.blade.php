@extends('frontend')
@section('content')
  <div id="all">

      <div id="content">
          <div class="container">

              <div class="col-md-12">

                  <ul class="breadcrumb">
                      <li><a href="{{ url('/user/cart/account') }}">Home</a></li>
                      <li><a href="#">Pesanan</a></li>
                      <li>Data Pesanan</li>
                  </ul>

              </div>

              <div class="col-md-3">
                @include('orders.sidebar')
              </div>

              <div class="col-md-9" id="customer-order">
                  <div class="box">
                    @if (Cart::total() > 0)
                      <h1>Pesanan Anda saat ini</h1>
                      <p class="lead">Berikut kami tampilkan review data pesanan Anda</p>

                      <hr>

                  <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="2">Produk</th>
                                        <th>Jumlah</th>
                                        <th>Harga</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($cart_content as $key => $d)
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <img src="{{ asset('/gambar/produk/' . $d->options->img) }}" alt="{{ $d->name }}">
                                                </a>
                                            </td>
                                            <td><a href="#">{{ $d->name }}</a>
                                            </td>
                                            <td>
                                                {{ $d->qty }}
                                            </td>
                                            <td>Rp. {{ number_format($d->price, 0,'.','.') }}</td>
                                            <td>Rp. {{ number_format($d->subtotal, 0,'.','.') }}</td>

                                        </tr>

                                    @endforeach

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="4">Total</th>
                                        <th>Rp. {{ number_format(Cart::total(), 0,'.','.') }}</th>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>
                        <!-- /.table-responsive -->
                          <div class="pull-right">
                            <a href="{{ url('cart-content') }}" class="btn btn-primary">Ubah Pesanan</a>
                            <a href="{{ url('user/cart/saveorder') }}" class="btn btn-primary">Simpan Pesanan</a>
                          </div>
                        <br>
                        <hr>
                      @endif
                        <h3>History Pesanan Sebelumnya</h3>
                        <div class='table-responsive'>
                          <table class='table table-striped table-bordered table-hover table-condensed'>
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Kode Pesanan</th>
                                <th>Total Tagihan</th>
                                <th>Tanggal Pesan</th>
                                <th>Detail</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($histori as $d)
                                <tr>
                                  <td>{{ $no++ }}</td>
                                  <td>{{ $d->kode }}</td>
                                  <td>{{ number_format($d->totalharga, 0,',','.') }}</td>
                                  <td>{{ $d->created_at }}</td>
                                  <td><a href="{{ url('user/cart/histori/'.$d->kode) }}" class="btn btn-sm btn-primary glyphicon glyphicon-envelope"> </a></td>
                                </tr>
                              @endforeach

                            </tbody>
                          </table>
                        </div>
                  </div>
              </div>

          </div>
          <!-- /.container -->
      </div>

@endsection
