@extends('frontend')
@section('content')
  <div id="all">

      <div id="content">
          <div class="container">

              <div class="col-md-12">

                  <ul class="breadcrumb">
                      <li><a href="{{ url('user/cart/account') }}">Home</a>
                      </li>
                      <li>Data Pelanggan</li>
                  </ul>

              </div>

              <div class="col-md-3">
                  @include('orders.sidebar')
              </div>

              <div class="col-md-9">
                  <div class="box">
                      <h3>Data Pelanggan</h3>

                      @if (!empty($pelanggan))
                      <p class="lead">Berikut data diri Anda.</p>
                      <p class="text-muted">Silakan periksa, sesuaikan data dengan kondisi Anda yang sebenar-benarnya.</p>
                      <table class='table table-striped table-bordered table-hover table-condensed'>
                        <tbody>
                          <tr>
                            <th>Nama Lengkap</th>
                            <td>{{ $pelanggan->namalengkap }}</td>
                          </tr>
                          <tr>
                            <th>Alamat</th>
                            <td>{{ $pelanggan->alamat }}</td>
                          </tr>
                          <tr>
                            <th>Kabupaten/ Kota</th>
                            <td>{{ $pelanggan->kota }}</td>
                          </tr>
                          <tr>
                            <th>Provinsi</th>
                            <td>{{ $pelanggan->provinsi }}</td>
                          </tr>
                          <tr>
                            <th>Kode Pos</th>
                            <td>{{ $pelanggan->kodepos }}</td>
                          </tr>
                          <tr>
                            <th>Keterangan</th>
                            <td>{{ $pelanggan->keterangan }}</td>
                          </tr>
                          <tr>
                            <th>No Handphone</th>
                            <td>{{ $pelanggan->nohp }}</td>
                          </tr>
                          <tr>
                            <th>E-mail</th>
                            <td>{{ $pelanggan->email }}</td>
                          </tr>
                        </tbody>
                      </table>
                      @endif
                      <a href="{{ url('user/cart/edit') }}" class="btn btn-sm btn-warning">Ubah Data</a>
                      @if (Cart::total() > 0)
                        <div class="pull-right">
                          <a href="{{ url('user/cart/orders') }}" class="btn btn-primary">Lanjut Pembayaran</a> <br>
                        </div>
                      @endif
                      <hr>

                  </div>
              </div>

          </div>
          <!-- /.container -->
      </div>

@endsection
