<h2>Konfirmasi Pesanan</h2>

<h4>Terima kasih pesanan Anda sudah kami terima, </h4>
					<hr>
					<h3>Berikut data pesanan Anda</h3>
					<div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="2">Produk</th>
                                        <th>Jumlah</th>
                                        <th>Harga Satuan</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($order as $key => $d)
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <img src="" alt="{{ $d->name }}">
                                                </a>
                                            </td>
                                            <td><a href="#">{{ getProduk($d->produk_id) }}</a>
                                            </td>
                                            <td>
                                                {{ $d->jumlah }}
                                            </td>
                                            <td>Rp. {{ number_format($d->harga, 0,'.','.') }}</td>
                                            <td>Rp. {{ number_format(($d->harga * $d->jumlah), 0,'.','.') }}</td>

                                        </tr>

                                    @endforeach

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="4">Total</th>
                                        <th>Rp. {{ number_format($total, 0,'.','.') }}</th>
                                    </tr>
                                </tfoot>
                            </table>
														
