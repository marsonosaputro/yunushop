@extends('frontend')
@section('content')
  <div id="all">

      <div id="content">
          <div class="container">

              <div class="col-md-12">

                  <ul class="breadcrumb">
                      <li><a href="{{ url('user/cart/account') }}">Home</a>
                      </li>
                      <li>Data Pelanggan</li>
                  </ul>

              </div>

              <div class="col-md-3">
                  @include('orders.sidebar')
              </div>

              <div class="col-md-9">
                  <div class="box">
                      <h3>Formulir  Data Pelanggan</h3>
                      <p class="lead">Lengkapi formulir berikut ini untuk menambahkan atau update data Anda!</p>

                      <form action="{{ url('user/cart/savepelanggan') }}" method="post">
                        {{ csrf_field() }}
                          <div class="row">
                              <div class="col-sm-6">
                                  <div class="form-group{{ $errors->has('namalengkap') ? ' has-error' : '' }}">
                                      {!! Form::label('namalengkap', 'Nama Lengkap') !!}
                                      <div>
                                          {!! Form::text('namalengkap', (!empty($pelanggan->namalengkap)) ? $pelanggan->namalengkap : Auth::user()->name, ['class' => 'form-control']) !!}
                                          <small class="text-danger">{{ $errors->first('namalengkap') }}</small>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-sm-6">
                                  <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
                                      {!! Form::label('alamat', 'Alamat') !!}
                                      {!! Form::text('alamat', (!empty($pelanggan->alamat)) ? $pelanggan->alamat : '', ['class' => 'form-control']) !!}
                                      <small class="text-danger">{{ $errors->first('alamat') }}</small>
                                  </div>
                              </div>
                          </div>
                          <!-- /.row -->

                          <div class="row">
                              <div class="col-sm-6">
                                  <div class="form-group{{ $errors->has('kota') ? ' has-error' : '' }}">
                                      {!! Form::label('kota', 'Kabupaten/ Kota') !!}
                                      {!! Form::select('kota', $kota, (!empty($pelanggan->kota)) ? $pelanggan->kota : '', ['class' => 'form-control']) !!}
                                      <small class="text-danger">{{ $errors->first('kota') }}</small>
                                  </div>
                              </div>
                              <div class="col-sm-6">
                                  <div class="form-group{{ $errors->has('provinsi') ? ' has-error' : '' }}">
                                      {!! Form::label('provinsi', 'Provinsi') !!}
                                      {!! Form::text('provinsi', (!empty($pelanggan->provinsi)) ? $pelanggan->provinsi : '', ['class'=>'form-control']) !!}
                                      <small class="text-danger">{{ $errors->first('provinsi') }}</small>
                                  </div>
                              </div>
                          </div>
                          <!-- /.row -->

                          <div class="row">
                              <div class="col-sm-12 col-md-6">
                                  <div class="form-group{{ $errors->has('kodepos') ? ' has-error' : '' }}">
                                      {!! Form::label('kodepos', 'Kode Pos') !!}
                                      {!! Form::text('kodepos', (!empty($pelanggan->kodepos)) ? $pelanggan->kodepos : '', ['class' => 'form-control']) !!}
                                      <small class="text-danger">{{ $errors->first('kodepos') }}</small>
                                  </div>
                              </div>
                              <div class="col-sm-12 col-md-6">
                                  <div class="form-group{{ $errors->has('keterangan') ? ' has-error' : '' }}">
                                      {!! Form::label('keterangan', 'Keterangan (alamat kirim yang berbeda)') !!}
                                      {!! Form::text('keterangan', (!empty($pelanggan->keterangan)) ? $pelanggan->keterangan : '', ['class' => 'form-control']) !!}
                                      <small class="text-danger">{{ $errors->first('keterangan') }}</small>
                                  </div>
                              </div>

                              <div class="col-sm-6">
                                  <div class="form-group{{ $errors->has('nohp') ? ' has-error' : '' }}">
                                      {!! Form::label('nohp', 'No Handphone') !!}
                                      {!! Form::text('nohp', (!empty($pelanggan->nohp)) ? $pelanggan->nohp : '', ['class' => 'form-control']) !!}
                                      <small class="text-danger">{{ $errors->first('nohp') }}</small>
                                  </div>
                              </div>
                              <div class="col-sm-6">
                                  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                      {!! Form::label('email', 'E-mail') !!}
                                      {!! Form::email('email', (!empty($pelanggan->email)) ? $pelanggan->email : Auth::user()->email, ['class' => 'form-control', 'placeholder' => 'eg: foo@bar.com']) !!}
                                      <small class="text-danger">{{ $errors->first('email') }}</small>
                                  </div>
                              </div>
                              <div class="col-sm-12 text-center">
                                  <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>

                              </div>
                          </div>
                      {!! Form::close() !!}

                  </div>
              </div>

          </div>
          <!-- /.container -->
      </div>

@endsection
