@extends('frontend')
@section('content')
	<div id="all">
		<div class="container">
			<div class="col-md-12">
				<div class="box">
					<h4>Terima kasih pesanan Anda sudah kami terima. Kami akan mengecek ketersediaan barang dan biaya kirim.
					Mohon untuk tidak mentransfer uang dulu sampai kami memberikan Anda informasi melalui SMS, whatsapp atau e-mail mengenai ketersediaan barang dan biaya kirim serta total uang yang harus Anda transfer.</h4>
					<hr>
					<h3>Berikut data pesanan Anda</h3>
					<div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="2">Produk</th>
                                        <th>Jumlah</th>
                                        <th>Harga</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($order as $key => $d)
                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <img src="" alt="{{ $d->name }}">
                                                </a>
                                            </td>
                                            <td><a href="#">{{ getProduk($d->produk_id) }}</a>
                                            </td>
                                            <td>
                                                {{ $d->jumlah }}
                                            </td>
                                            <td>Rp. {{ number_format($d->harga, 0,'.','.') }}</td>
                                            <td>Rp. {{ number_format(($d->harga * $d->jumlah), 0,'.','.') }}</td>

                                        </tr>

                                    @endforeach

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="4">Total</th>
                                        <th>Rp. {{ number_format($total, 0,'.','.') }}</th>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>
				</div>
			</div>
		</div>
	</div>



@endsection
