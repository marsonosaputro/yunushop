@extends('frontend')
@section('content')
  <div id="all">

      <div id="content">
          <div class="container">

              <div class="col-md-12">

                  <ul class="breadcrumb">
                      <li><a href="{{ url('/user/cart/account') }}">Home</a></li>
                      <li><a href="#">Pesanan</a></li>
                      <li>Data Pesanan</li>
                  </ul>

              </div>

              <div class="col-md-3">
                @include('orders.sidebar')
              </div>

              <div class="col-md-9" id="customer-order">
                  <div class="box">
                      <h3>History Pesanan Sebelumnya</h3>

                      <p class="lead">Berikut kami tampilkan review data pesanan Anda</p>


                      <hr>

                        <div class='table-responsive'>
                          <table class='table table-striped table-bordered table-hover table-condensed'>
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Nama Produk</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th>Total</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($histori as $d)
                                <tr>
                                  <td>{{ $no++ }}</td>
                                  <td>{{ getProduk($d->produk_id) }}</td>
                                  <td>{{ $d->jumlah }}</td>
                                  <td>{{ number_format($d->harga, 0,',','.') }}</td>
                                  <td>{{ number_format(($d->harga * $d->jumlah), 0,',','.') }}</td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                        <div class="pull-right">
                          <a href="{{ url('user/cart/orders') }}" class="btn btn-sm btn-danger">Back</a>
                        </div>
                        <br><br>
                  </div>
              </div>

          </div>
          <!-- /.container -->
      </div>

@endsection
