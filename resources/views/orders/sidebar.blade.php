<div class="panel panel-default sidebar-menu">

    <div class="panel-heading">
        <h3 class="panel-title">Data Pelanggan</h3>
    </div>

    <div class="panel-body">

        <ul class="nav nav-pills nav-stacked">
            <li class="{{ Request::is('user/cart/account') ? 'active' : '' }}">
                <a href="{{ url('user/cart/account') }}"><i class="fa fa-user"></i> Data Pelanggan</a>
            </li>
            <li class="{{ Request::is('user/cart/orders') ? 'active' : '' }}">
                <a href="{{ url('user/cart/orders') }}"><i class="fa fa-list"></i> Data Pesanan</a>
            </li>
            <li>
                <a href="{{ url('logout') }}"><i class="fa fa-sign-out"></i> Logout</a>
            </li>
        </ul>
    </div>

</div>
