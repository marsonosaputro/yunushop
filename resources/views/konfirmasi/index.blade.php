@extends('admin_template')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Data Konfirmasi Pembayaran</h3>
    </div>
    <div class="panel-body">
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th>No HP</th>
              <th>Bank</th>
              <th>No. Rekening</th>
              <th>Atas Nama</th>
              <th colspan="2">Nominal</th>

            </tr>
          </thead>
          <tbody>
            @foreach ($retur as $d)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $d->nama }}</td>
                <td>{{ $d->alamat }}</td>
                <td>{{ $d->nohp }}</td>
                <td>{{ $d->bank }}</td>
                <td>{{ $d->rekening }}</td>
                <td>{{ $d->atasnama }}</td>
                <td>{{ number_format($d->nominal,0,',','.') }}</td>
                <td>
                  {!! Form::open(array('url'=>'konfirmasi/'.$d->id, 'method'=>'delete')) !!}
                  {!! Form::hidden('_delete', 'DELETE') !!}
                  <button type="submit" onclick="javascript: return confirm('Yakin akan di hapus?')" class="btn btn-xs btn-danger glyphicon glyphicon-remove"></button>
                  {!! Form::close() !!}
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer">

    </div>
  </div>

@endsection
