@extends('admin_template')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">
          <a href="{{ url('admin/ongkir/create') }}" class="btn btn-primary">Tambah</a>
      </h3>
    </div>
    <div class="panel-body">
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <thead>
            <tr>
              <th>No</th>
              <th>Kota</th>
              <th>Ongkir</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($ongkir as $d)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $d->kota }}</td>
                <td>{{ number_format($d->harga,0,',','.') }}</td>
                <td>
                  {!! Form::open(array('url'=>'admin/ongkir/'.$d->id, 'method'=>'delete')) !!}
                  {!! Form::hidden('_delete', 'DELETE') !!}
                  <a href="{{ url('admin/ongkir').'/'.$d->id.'/edit' }}" class="btn btn-info btn-sm glyphicon glyphicon-edit"></a>
                  <button type="submit" onclick="javascript: return confirm('Yakin akan di hapus?')" class="btn btn-sm btn-danger glyphicon glyphicon-remove"></button>
                  {!! Form::close() !!}
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="pull-right">
          {!! $ongkir->render() !!}
      </div>

    </div>
    <div class="panel-footer">

    </div>
  </div>

@endsection
