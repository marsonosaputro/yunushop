@extends('admin_template')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Tambah Daftar Ongkir</h3>
    </div>
    <div class="panel-body">
      {!! Form::open(['method' => 'POST', 'route' => 'admin.ongkir.store', 'class' => 'form-horizontal']) !!}

          <div class="form-group{{ $errors->has('kota') ? ' has-error' : '' }}">
              {!! Form::label('kota', 'Kota', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('kota', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('kota') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
              {!! Form::label('harga', 'Ongkir', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::number('harga', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('harga') }}</small>
              </div>
          </div>

          <div class="btn-group pull-right">
              <a href="{{ url('admin/ongkir') }}" class="btn btn-warning">Batal</a>
              {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>
      {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>

@endsection
