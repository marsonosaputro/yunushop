@extends('frontend')
@section('content')
    <div id="content">
    <div class="container">
    <div class="col-sm-12">
    <ul class="breadcrumb">
    <li><a href="{{ url('/') }}">Home</a>
    </li>
    <li>Retur Barang</li>
    </ul>
    </div>
            <!-- *** LEFT COLUMN ***
         _________________________________________________________ -->
    <div class="col-sm-9" id="blog-listing">
    <div class="post">

      <h3>Konfirmasi Retur Barang</h3>
      <p>
        Isi lengkap Formulir berikut untuk konfirmasi retur barang!
      </p>
      @if (session()->has('flash_notification.message'))
          <div class="alert alert-{{ session()->get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! session()->get('flash_notification.message') !!}
          </div>
      @endif

      {!! Form::open(['method' => 'POST', 'route'=>'confirm.store', 'class' => 'form-horizontal']) !!}

          <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
              {!! Form::label('nama', 'Nama Lengkap', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('nama', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('nama') }}</small>
              </div>
          </div>
          <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
              {!! Form::label('alamat', 'Alamat', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('alamat', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('alamat') }}</small>
              </div>
          </div>
          <div class="form-group{{ $errors->has('nohp') ? ' has-error' : '' }}">
              {!! Form::label('nohp', 'Nomor HP', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('nohp', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('nohp') }}</small>
              </div>
          </div>
          <div class="form-group{{ $errors->has('namabarang') ? ' has-error' : '' }}">
              {!! Form::label('namabarang', 'Nama Barang', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('namabarang', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('namabarang') }}</small>
              </div>
          </div>
          <div class="form-group{{ $errors->has('alasan') ? ' has-error' : '' }}">
              {!! Form::label('alasan', 'Alasan Retur', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('alasan', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('alasan') }}</small>
              </div>
          </div>
          <div class="form-group{{ $errors->has('pilihan') ? ' has-error' : '' }}">
              {!! Form::label('pilihan', 'Pilihan Retur', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::select('pilihan', ['uang'=>'Uang', 'barang'=>'Barang'], null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('pilihan') }}</small>
              </div>
          </div>

          <div class="btn-group pull-right">
              {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
              {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>
      {!! Form::close() !!}

    <br>
    <hr>
    <p>
    <h4>Bagikan ke:</h4>
                    <!-- AddToAny BEGIN -->
    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
    <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
    <a class="a2a_button_facebook"></a>
    <a class="a2a_button_twitter"></a>
    <a class="a2a_button_google_plus"></a>
    <a class="a2a_button_whatsapp"></a>
    <a class="a2a_button_line"></a>
    <a class="a2a_button_sms"></a>
    </div>
    <script async src="https://static.addtoany.com/menu/page.js"></script>
                    <!-- AddToAny END -->
    </p>
    </div>
    </div>
            <!-- /.col-md-9 -->

            <!-- *** LEFT COLUMN END *** -->
    <div class="col-md-3">
@include('frontend.statisSidebar')
    </div>
    </div>
        <!-- /.container -->
    </div>
@endsection
