<!DOCTYPE html>
  <html>
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Butik Anis</title>
    <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
  <link href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
  <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
  <link href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
  <link href="{{ asset("/bower_components/admin-lte/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="{{ asset('bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
  <div class="login-box">
  <div class="login-logo">
  <a href="#"><b>Butik </b>Anis</a>
  </div><!-- /.login-logo -->
  <div class="login-box-body">
  <p class="login-box-msg">Silakan login dengan e-mail dan password Anda!</p>
  <form role="form" method="POST" action="{{ url('/login') }}">
  {{ csrf_field() }}
  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
  <label for="email">E-mail</label>
  <div>
  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
@if ($errors->has('email'))
  <span class="help-block">
  <strong>{{ $errors->first('email') }}</strong>
  </span>
@endif
  </div>
  </div>
  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
  <label for="password">Password</label>
  <div>
  <input id="password" type="password" class="form-control" name="password">
@if ($errors->has('password'))
  <span class="help-block">
  <strong>{{ $errors->first('password') }}</strong>
  </span>
@endif
  </div>
  </div>
  <div class="form-group">
  <div>
  <div class="checkbox">
  <label>
  <input type="checkbox" name="remember"> Remember Me
  </label>
  </div>
  </div>
  </div>
  <div class="form-group">
  <div>
  <button type="submit" class="btn btn-primary">
  <i class="fa fa-btn fa-sign-in"></i> Login
  </button>
  </div>
  </div>
  </form>
  </div><!-- /.login-box-body -->
  </div><!-- /.login-box -->
    <!-- jQuery 2.1.3 -->
  <script src="{{ asset ("/bower_components/admin-lte/plugins/jQuery/jQuery-2.2.0.min.js") }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
  <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
  <script src="{{ asset ("/bower_components/admin-lte/dist/js/app.min.js") }}" type="text/javascript"></script>
    <!-- CK Editor -->
  <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
  <script src="{{ asset('/bower_components/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
  <script>
  $(function () {
  $('input').iCheck({
  checkboxClass: 'icheckbox_square-blue',
  radioClass: 'iradio_square-blue',
  increaseArea: '20%' // optional
  });
  });
  </script>
  </body>
  </html>
