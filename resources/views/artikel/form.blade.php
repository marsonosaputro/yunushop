@include('tinymce')
    <div class="form-group{{ $errors->has('kategori_artikel_id') ? ' has-error' : '' }}">
    {!! Form::label('kategori_artikel_id', 'Kategori Artikel', ['class' => 'col-md-2']) !!}
    <div class="col-md-6">
    {!! Form::select('kategori_artikel_id', $kategori, NULL, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('kategori_artikel_id') }}</small>
    </div>
    </div>

    <div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
    {!! Form::label('judul', 'Judul Artikel', ['class' => 'col-md-2']) !!}
    <div class="col-md-6">
    {!! Form::text('judul', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('judul') }}</small>
    </div>
    </div>

    <div class="form-group{{ $errors->has('artikel') ? ' has-error' : '' }}">
    {!! Form::label('artikel', 'Isi Artikel', ['class' => 'col-md-2']) !!}
    <div class="col-md-8">
    {!! Form::textarea('artikel', null, ['class' => 'form-control']) !!}
    <small class="text-danger">{{ $errors->first('artikel') }}</small>
    </div>
    </div>

    <div class="form-group{{ $errors->has('gambar') ? ' has-error' : '' }}">
    {!! Form::label('gambar', 'Gambar Artikel', ['class' => 'col-md-2']) !!}
    <div class="col-md-6">
    {!! Form::file('gambar', ['class' => 'form-horizontal']) !!}
    <p class="help-block">Ukuran 1000 x 400px</p>
    <small class="text-danger">{{ $errors->first('gambar') }}</small>
    </div>
    </div>
