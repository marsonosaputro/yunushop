@extends('admin_template')
@section('content')
    <div class="panel panel-default">
    <div class="panel-heading">
    <h3 class="panel-title">
    <a href="{{ url('admin/artikel/create') }}" class="btn btn-primary">Tambah</a>
    </h3>
    </div>
    <div class="panel-body">
    <div class='table-responsive'>
    <table class='table table-striped table-bordered table-hover table-condensed'>
    <thead>
    <tr>
    <th>No</th>
    <th>Judul</th>
    <th>Kategori</th>
    <th>Hits</th>
    <th>Penulis</th>
    <th>Aksi</th>
    </tr>
    </thead>
    <tbody>
@foreach($artikel as $key => $d)
    {!! Form::open(array('url'=>'admin/artikel/'.$d->id, 'method'=>'delete')) !!}
    {!! Form::hidden('_delete', 'DELETE') !!}
    <tr>
    <td>{{ $no++ }}</td>
    <td>{{ $d->judul }}</td>
    <td>{{ baca_kategori_artikel($d->kategori_artikel_id) }}</td>
    <td>{{ $d->hits }}</td>
    <td>{{ $d->penulis }}</td>
    <td>
    <a href="{{ url('admin/artikel').'/'.$d->id.'/edit' }}" class="btn btn-info btn-sm glyphicon glyphicon-edit"></a>
    <button type="submit" onclick="javascript: return confirm('Yakin akan di hapus?')" class="btn btn-sm btn-danger glyphicon glyphicon-remove"></button>
    </td>
    </tr>
    {!! Form::close() !!}
@endforeach
    </tbody>
    </table>
    </div>
    {!! $artikel->render() !!}
    </div>
    </div>
@endsection
