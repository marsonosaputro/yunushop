@extends('admin_template')
@section('content')
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">{{ $page_title }}</h3>
      </div>
      <div class="panel-body">
        {!! Form::model($slideshow, ['route' => ['admin.slideshow.update', $slideshow->id], 'method' => 'PUT', 'files'=>true, 'class'=>'form-horizontal']) !!}

        <div class="form-group">
            {!! Form::label('img', 'Gambar Sebelumnya', ['class' => 'col-md-2']) !!}
                <div class="col-md-6">
                    <img src="{{ URL::asset('/gambar/slideshow/' . $slideshow->gambar) }}" class="img img-responsive" style="width:300px;">
                </div>
        </div>
        <hr>
        @include('slideshow.form')

        <div class="form-group">
            <div class="col-md-6 col-md-offset-2">
                {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
                <a href="{{ url('admin/slideshow') }}" class="btn btn-default">Batal</a>
            </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
@endsection
