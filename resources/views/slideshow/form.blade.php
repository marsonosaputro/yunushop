<div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
    {!! Form::label('judul', 'Judul', ['class' => 'col-md-2']) !!}
    <div class="col-md-6">
        {!! Form::text('judul', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('judul') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('gambar') ? ' has-error' : '' }}">
    {!! Form::label('gambar', 'Gambar', ['class' => 'col-md-2']) !!}
        <div class="col-md-6">
            {!! Form::file('gambar') !!}
            <p class="help-block">Ukuran: 1500 x 600</p>
            <small class="text-danger">{{ $errors->first('gambar') }}</small>
        </div>
</div>

<div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
    {!! Form::label('link', 'Link', ['class' => 'col-md-2']) !!}
    <div class="col-md-6">
        {!! Form::text('link', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('link') }}</small>
    </div>
</div>
