@extends('admin_template')
@section('content')
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">
            <a href="{{ url('admin/user/create') }}" class="btn btn-primary">Tambah</a>
        </h3>
      </div>
      <div class="panel-body">
          <div class='table-responsive'>
            <table class='table table-striped table-bordered table-hover table-condensed'>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Lengkap</th>
                  <th>Email</th>
                  <th>Level</th>
                  <th>Aktif Sejak</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($user as $key => $d)
                      {!! Form::open(array('url'=>'admin/user/'.$d->id, 'method'=>'delete')) !!}
                      {!! Form::hidden('_delete', 'DELETE') !!}
                      <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $d->name }}</td>
                        <td>{{ $d->email }}</td>
                        <td>{{ $d->level }}</td>
                        <td>{{ $d->created_at }}</td>
                        <td>
                            <a href="{{ url('admin/user').'/'.$d->id.'/edit' }}" class="btn btn-info btn-sm glyphicon glyphicon-edit"></a>
                            <button type="submit" onclick="javascript: return confirm('Yakin akan di hapus?')" class="btn btn-sm btn-danger glyphicon glyphicon-remove"></button>
                        </td>
                      </tr>
                      {!! Form::close() !!}
                  @endforeach
              </tbody>
            </table>
          </div>
      </div>
    </div>
@endsection
