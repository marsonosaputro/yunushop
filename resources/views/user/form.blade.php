<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label('name', 'Nama Lengkap', ['class' => 'col-md-2']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('name') }}</small>
    </div>
</div>
 <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
     {!! Form::label('email', 'Alamat Email', ['class' =>'col-md-2']) !!}
     <div class="col-md-6">
         {!! Form::email('email', null, ['class' => 'form-control']) !!}
         <small class="text-danger">{{ $errors->first('email') }}</small>
     </div>
 </div>
 <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
     {!! Form::label('password', 'Password', ['class' => 'col-md-2']) !!}
         <div class="col-md-6">
             {!! Form::password('password', ['class' => 'form-control']) !!}
             <small class="text-danger">{{ $errors->first('password') }}</small>
         </div>
 </div>
 <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
     {!! Form::label('password_confirmation', 'Konfirmasi Password', ['class' => 'col-md-2']) !!}
         <div class="col-md-6">
             {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
             <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
         </div>
 </div>
