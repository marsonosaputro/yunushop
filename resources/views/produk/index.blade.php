@extends('admin_template')
@section('content')
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">
            <a href="{{ url('admin/produk/create') }}" class="btn btn-primary">Tambah</a>
        </h3>
      </div>
      <div class="panel-body">
          <div class='table-responsive'>
            <table class='table table-striped table-bordered table-hover table-condensed'>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Produk</th>
                  <th>Stock</th>
                  <th>Harga beli</th>
                  <th>Harga jual</th>
                  <th>Berat</th>
                  <th>Gambar</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach($produk as $key => $d)
                    {!! Form::open(array('url'=>'admin/produk/'.$d->id, 'method'=>'delete')) !!}
                    {!! Form::hidden('_delete', 'DELETE') !!}
                    <tr>
                      <td>{{ $no++ }}</td>
                      <td>{{ $d->judul }}</td>
                      <td>{{ $d->stock }}</td>
                      <td>{{ number_format($d->hargabeli, 0,',','.') }}</td>
                      <td>{{ number_format($d->harga, 0,',','.') }}</td>
                      <td>{{ $d->berat }}</td>
                      <td><img src="{{ URL::asset('/gambar/produk/' . $d->gambar) }}" class="img img-responsive" style="width:150px"></td>
                      <td>
                          <a href="{{ url('admin/produk').'/'.$d->id.'/edit' }}" class="btn btn-info btn-sm glyphicon glyphicon-edit"></a>
                          <button type="submit" onclick="javascript: return confirm('Yakin akan di hapus?')" class="btn btn-sm btn-danger glyphicon glyphicon-remove"></button>
                      </td>
                    </tr>
                    {!! Form::close() !!}
                @endforeach
              </tbody>
            </table>
          </div>
          <div class="pull-right" style="margin-top:-20px">
              {!! $produk->render() !!}
          </div>

      </div>
    </div>
@endsection
