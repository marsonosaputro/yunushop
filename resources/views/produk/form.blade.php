@include('tinymce')
<div class="form-group{{ $errors->has('kategori_id') ? ' has-error' : '' }}">
    {!! Form::label('kategori_id', 'Kategori Produk', ['class'=>'col-md-2']) !!}
    <div class="col-md-6">
        {!! Form::select('kategori_id', $kategori, '', ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('kategori_id') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
    {!! Form::label('judul', 'Judul Produk', ['class' => 'col-md-2']) !!}
    <div class="col-md-6">
        {!! Form::text('judul', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('judul') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
    {!! Form::label('deskripsi', 'Keterangan', ['class' => 'col-md-2']) !!}
    <div class="col-md-8">
        {!! Form::textarea('deskripsi', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('deskripsi') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('stock') ? ' has-error' : '' }}">
    {!! Form::label('stock', 'Stock', ['class' => 'col-md-2']) !!}
    <div class="col-md-6">
        {!! Form::text('stock', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('stock') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('hargabeli') ? ' has-error' : '' }}">
    {!! Form::label('hargabeli', 'Harga Beli', ['class' => 'col-md-2']) !!}
    <div class="col-md-6">
        {!! Form::text('hargabeli', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('hargabeli') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('harga') ? ' has-error' : '' }}">
    {!! Form::label('harga', 'Harga Jual', ['class' => 'col-md-2']) !!}
    <div class="col-md-6">
        {!! Form::text('harga', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('harga') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('gambar') ? ' has-error' : '' }}">
    {!! Form::label('gambar', 'Gambar', ['class'=>'col-md-2']) !!}
    <div class="col-md-6">
        {!! Form::file('gambar', ['class' => 'form-horizontal']) !!}
        <small class="text-danger">{{ $errors->first('gambar') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('berat') ? ' has-error' : '' }}">
    {!! Form::label('berat', 'Berat produk (gram)', ['class' => 'col-md-2']) !!}
    <div class="col-sm-6">
        {!! Form::number('berat', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('berat') }}</small>
    </div>
</div>
