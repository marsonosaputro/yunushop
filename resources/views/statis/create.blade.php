@extends('admin_template')
@section('content')
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Tambah Statis</h3>
      </div>
      <div class="panel-body">
          {!! Form::open(['method' => 'POST', 'route' => 'admin.statis.store', 'class' => 'form-horizontal', 'files'=>true]) !!}

              @include('statis.form')

              <div class="form-group">
                  <div class="col-md-6 col-md-offset-2">
                      {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
                      <a href="{{ url('admin/statis') }}" class="btn btn-default">Batal</a>
                  </div>
              </div>
          {!! Form::close() !!}

      </div>
    </div>
@endsection
