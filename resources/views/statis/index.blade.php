@extends('admin_template')
@section('content')
    <div class="panel panel-default">
      <div class="panel-heading">
        <a href="{{ url('admin/statis/create') }}" class="btn btn-primary">Tambah</a>
      </div>
      <div class="panel-body">
          <div class='table-responsive'>
            <table class='table table-striped table-bordered table-hover table-condensed'>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Judul</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach($statis as $key => $d)
                    {!! Form::open(array('url'=>'admin/statis/'.$d->id, 'method'=>'delete')) !!}
                    {!! Form::hidden('_delete', 'DELETE') !!}
                    <tr>
                      <td>{{ $no++ }}</td>
                      <td>{{ $d->judul }}</td>
                      <td>
                          <a href="{{ url('admin/statis').'/'.$d->id.'/edit' }}" class="btn btn-info btn-sm glyphicon glyphicon-edit"></a>
                          <button type="submit" onclick="javascript: return confirm('Yakin akan di hapus?')" class="btn btn-sm btn-danger glyphicon glyphicon-remove"></button>
                      </td>
                    </tr>
                    {!! Form::close() !!}
                @endforeach
              </tbody>
            </table>
          </div>
      </div>
    </div>

@endsection
