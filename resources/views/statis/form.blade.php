@include('tinymce')
<div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
    {!! Form::label('judul', 'Judul', ['class'=>'col-md-2']) !!}
    <div class="col-md-6">
        {!! Form::text('judul', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('judul') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('isi') ? ' has-error' : '' }}">
    {!! Form::label('isi', 'Isi Halaman', ['class'=>'col-md-2']) !!}
    <div class="col-md-8">
        {!! Form::textarea('isi', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('isi') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('gambar') ? ' has-error' : '' }}">
    {!! Form::label('gambar', 'Gambar', ['class'=>'col-md-2']) !!}
    <div class="col-md-6">
        {!! Form::file('gambar', ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('gambar') }}</small>
    </div>
</div>
