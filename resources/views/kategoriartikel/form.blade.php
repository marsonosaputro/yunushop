<div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
    {!! Form::label('judul', 'Kategori Artikel', ['class'=>'col-md-2']) !!}
    <div class="col-md-6">
        {!! Form::text('judul', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('judul') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('keterangan') ? ' has-error' : '' }}">
    {!! Form::label('keterangan', 'Keterangan', ['class'=>'col-md-2']) !!}
    <div class="col-md-6">
        {!! Form::textarea('keterangan', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('keterangan') }}</small>
    </div>
</div>
