@extends('admin_template')
@section('content')
	<div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">
            Detail Pesanan
        </h3>
      </div>
      <div class="panel-body">
				<h4>Data Pelanggan</h4>
				<div class='table-responsive'>
				  <table class='table table-striped table-bordered table-hover table-condensed'>
				    <thead>
				      <tr>
				        <th style="width:15%">Nama Lengkap</th><td>{{ $pemesan->namalengkap }}</td>
				        <th style="width:15%">No Handphone</th><td>{{ $pemesan->nohp }}</td>
				      </tr>
							<tr>
				        <th>Alamat</th><td>{{ $pemesan->alamat }}</td>
				        <th>E-mail</th><td>{{ $pemesan->email }}</td>
				      </tr>
							<tr>
				        <th>Kabupaten/ Kota</th><td>{{ $pemesan->kota }} {{ $pemesan->provinsi }} kdpos {{ $pemesan->kodepos }}</td>
				        <th rowspan="2">Status</th>
				        <td rowspan="2">
				        	{!! Form::open(['method' => 'POST', 'url' => 'admin/ubahstatus'] ) !!}
				        		{!! Form::hidden('kode', $total->kode) !!}
				        	    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
				        	        {!! Form::select('status', $option, $total->status, ['id' => 'status', 'class' => 'form-control', 'style'=>'width:50%']) !!}
				        	    	{!! Form::submit("Simpan Status", ['class' => 'btn btn-success']) !!}
				        	    </div>

				        	{!! Form::close() !!}
				        </td>
				      </tr>
							<tr>
				        <th>Keterangan</th><td>{{ $pemesan->keterangan }}</td>
				      </tr>

				    </thead>
				  </table>
				</div>
				<hr>
				<h4>Data Pesanan</h4>
          <div class='table-responsive'>
            <table class='table table-striped table-bordered table-hover table-condensed'>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Produk</th>
                  <th>Jumlah</th>
                  <th>Harga</th>
									<th>SubTotal</th>
                </tr>
              </thead>
              <tbody>
								@foreach($pesanan as $key => $d)
									<tr>
										<td>{{ $no++ }}</td>
		                <td>{{ getProduk($d->produk_id) }}</td>
		                <td>{{ $d->jumlah }}</td>
		                <td>{{ number_format($d->harga, 0,',','.') }}</td>
										<td>{{ number_format($d->jumlah * $d->harga, 0,',','.') }}</td>
									</tr>
								@endforeach
								<tr>
									<th colspan="4">Total Harus Dibayar</th>
									<th>{{number_format($total->totalharga, 0,',','.')  }}	</th>
								</tr>
              </tbody>
            </table>
          </div>
      </div>
    </div>
@endsection
