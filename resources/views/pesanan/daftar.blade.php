@extends('admin_template')
@section('content')
	<div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">
            Daftar Pesanan
        </h3>
      </div>
      <div class="panel-body">
          <div class='table-responsive'>
            <table class='table table-striped table-bordered table-hover table-condensed'>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Pesanan</th>
                  <th>Nama Pelanggan</th>
                  <th>Tanggal Pesanan</th>
									<th>Status</th>
                  <th>View</th>
                  <th>Batalkan</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($pesanan as $d)
                	<tr>
                		<td>{{ $no++ }}</td>
                		<td>{{ $d->kode }}</td>
                		<td>{{ getNama($d->user_id) }}</td>
                		<td>{{ $d->created_at }}</td>
										<td>{{ $d->status }}</td>
                		<td>
                			<a href="{{ url('admin/pesanan/detail/'.$d->kode) }}" class="btn btn-primary btn-sm glyphicon glyphicon-zoom-in"></a>
                    </td>
                    <td>
                      {!! Form::open(['method' => 'POST', 'url' => 'admin/hapuspesanan', 'class'=>'form-horisontal']) !!}
                            {!! Form::hidden('kode', $d->kode) !!}
                            <button type="submit" onclick="javascript: return confirm('Yakin akan di hapus?')" class="btn btn-danger btn-sm glyphicon glyphicon-trash"></button>
                          </div>

                      {!! Form::close() !!}
                    </td>
                	</tr>
                @endforeach
              </tbody>
            </table>
          </div>
      </div>
    </div>
@endsection
