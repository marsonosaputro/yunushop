@extends('admin_template')
@section('content')
	<div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">
            Daftar Pesanan
        </h3>
      </div>
      <div class="panel-body">
          <div class='table-responsive'>
            <table class='table table-striped table-bordered table-hover table-condensed'>
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Produk</th>
                  <th>Jumlah</th>
                  <th>Harga</th>
									<th>Total</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($penjualan as $d)
                  <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ getProduk($d->produk_id) }}</td>
                    <td>{{ $d->jumlah }}</td>
                    <td>{{ number_format($d->harga,0,',','.') }}</td>
                    <td>{{ number_format($d->jumlah * $d->harga,0,',','.') }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
      </div>
    </div>
@endsection
