<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yunushop Online">
    <meta name="author" content="Yunus">
    <meta name="keywords" content="online shop">

    <title>
        {{ isset($page_title) ? $page_title : 'Butik Anis' }}
    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="{{ asset('frontend/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/owl.theme.css') }}" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="{{ asset('frontend/css/style.default.css') }}" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="{{ asset('frontend/css/custom.css') }}" rel="stylesheet">

    <script src="{{ asset('frontend/js/respond.min.js') }}"></script>
    <script src="https://use.fontawesome.com/3f23df9e0b.js"></script>

    <link rel="shortcut icon" href="{{ asset('frontend/favicon.png') }}">



</head>

<body>

    <!-- *** TOPBAR ***
 _________________________________________________________ -->
    <div id="top">
        <div class="container">
            <div class="col-md-6 offer" data-animate="fadeInDown">
                <a href="{{ url('wellcome') }}" class="btn btn-success btn-sm" data-animate-hover="shake">Selamat Datang!</a>  <a href="{{ url('gallery-produk') }}">Dapatkan produk berkualitas di toko kami.</a>
            </div>
            <div class="col-md-6" data-animate="fadeInDown">
                <ul class="menu">
                  @if(Auth::check())
                    <li><a href="{{ url('/logout') }}" >Logout</a></li>
                    <li><a href="{{ url('/user/cart/account') }}">Area Pelanggan</a></li>
                  @else
                    <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a></li>
                  @endif
                    <li><a href="{{ url('about') }}">Profil</a></li>
                    <li><a href="{{ url('cara-belanja') }}">Cara Belanja</a></li>
                    <li><a href="{{ route('konfirmasi.create') }}">Konfirmasi Pembayaran</a></li>
                    <li><a href="{{ url('kontak') }}">Kontak</a></li>
                </ul>
            </div>
        </div>
        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
            <div class="modal-dialog modal-sm">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="Login">Pelanggan Login</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{ url('/login') }}" method="post">
                          {{ csrf_field() }}
                          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                              <label for="email">E-mail</label>

                              <div>
                                  <input id="email-modal"  type="email" class="form-control" name="email" value="{{ old('email') }}">

                                  @if ($errors->has('email'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                              <label for="password">Password</label>

                              <div>
                                  <input id="password-modal" type="password" class="form-control" name="password">

                                  @if ($errors->has('password'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                            <p class="text-center">
                                <button class="btn btn-primary"><i class="fa fa-sign-in"></i> Login</button>
                            </p>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- *** TOP BAR END *** -->

    <!-- *** NAVBAR ***
 _________________________________________________________ -->

    <div class="navbar navbar-default yamm" role="navigation" id="navbar">
        <div class="container">
            <div class="navbar-header">

                <a class="navbar-brand home" href="{{ url('/') }}" data-animate-hover="bounce">
                    <img src="{{ asset('frontend') }}/img/logo.png" alt="YunusSHOP" class="hidden-xs img img-responsive" style="width:80px">
                    <img src="{{ asset('frontend') }}/img/logo.png" alt="YunusSHOP" class="img img-responsive visible-xs img img-responsive" style="width:85px;"><span class="sr-only">Butik Anis</span>
                </a>

                <div class="navbar-buttons">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                      <span style="font-weight: bold;">MENU</span>
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-align-justify"></i>
                    </button>
                </div>

            </div>
            <!--/.navbar-header -->

            <div class="navbar-collapse collapse" id="navigation">

                <ul class="nav navbar-nav navbar-left">
                    <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{ url('/') }}">Home</a></li>
                    <li class="{{ Request::is('about') ? 'active' : '' }}"><a href="{{ url('/about') }}">Profil</a></li>
                    <li class="{{ Request::is('gallery-produk') ? 'active' : '' }}"><a href="{{ url('/gallery-produk') }}">Produk</a></li>
                    <li class="{{ Request::is('blog') ? 'active' : '' }}"><a href="{{ url('/blog') }}">Artikel</a></li>
                    <li class="{{ Request::is('cara-belanja') ? 'active' : '' }}"><a href="{{ url('/cara-belanja') }}">Cara Belanja</a></li>
                    <li class="{{ Request::is('retur') ? 'active' : '' }}"><a href="{{ url('/retur') }}">Retur</a></li>
                    <li class="{{ Request::is('kontak') ? 'active' : '' }}"><a href="{{ url('/kontak') }}">Kontak</a></li>
                    @if (Auth::check())
                      <li class="{{ Request::is('user/cart/account') ? 'active' : '' }}"><a href="{{ url('user/cart/account') }}">Area Pelanggan</a></li>
                    @endif
                </ul>

            </div>
            <!--/.nav-collapse -->

            <div class="navbar-buttons">

                <div class="navbar-collapse collapse right" id="basket-overview">
                    <a href="{{ url('/cart-content') }}" class="btn btn-primary navbar-btn"><i class="fa fa-shopping-cart"></i><span class="hidden-sm">{{ Cart::count() }} barang</span></a>
                </div>
                <!--/.nav-collapse -->

                <!--
                <div class="navbar-collapse collapse right" id="search-not-mobile">
                    <button type="button" class="btn navbar-btn btn-primary" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
                    </button>
                </div>
              -->
            </div>

            <div class="collapse clearfix" id="search">

                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">

			<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>

		    </span>
                    </div>
                </form>

            </div>
            <!--/.nav-collapse -->

        </div>
        <!-- /.container -->
    </div>
    <!-- /#navbar -->

    <!-- *** NAVBAR END *** -->
    @yield('content')

        <!-- *** FOOTER ***
 _________________________________________________________ -->
        <div id="footer" data-animate="fadeInUp">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <h4>Halaman Informasi</h4>

                        <ul>
                            <li><a href="{{ url('/about') }}">Profil</a></li>
                            <li><a href="{{ url('/cara-belanja') }}">Cara Belanja</a></li>
                            <li><a href="{{ url('kontak') }}">Kontak</a></li>
                        </ul>

                        <hr>

                        <h4>Area Pelanggan</h4>

                        <ul>
                            <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a>
                            </li>

                        </ul>

                        <hr class="hidden-md hidden-lg hidden-sm">

                    </div>
                    <!-- /.col-md-3 -->

                    <div class="col-md-3 col-sm-6">

                        <h4>Kategori Produk</h4>

                        <ul>
                            @foreach ($kategoriproduk as $d)
                                <li><a href="{{ url('kategori-produk/'.$d->id.'/'.$d->seo) }}">{{ $d->judul }}</a></li>
                            @endforeach
                        </ul>
                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- /.col-md-3 -->

                    <div class="col-md-3 col-sm-6">
                        <h4>Kategori Artikel</h4>
                        <ul>
                            @foreach ($kategoriartikel as $d)
                                <li><a href="{{ url('blog/kategori/'.$d->id) }}">{{ $d->judul }}</a></li>
                            @endforeach
                        </ul>


                    </div>
                    <!-- /.col-md-3 -->
                    <div class="col-md-3 col-sm-6">
                        <h4>Kontak Kami</h4>
                        <i class="fa fa-map-marker"></i> Garen RT 02/ IV, Pandeyan, Ngemplak, Boyolali 57375, Jawa Tengah
                        <br><i class="fa fa-phone-square"></i> (0271)720611
                        <br><i class="fa fa-tablet"></i> 08562689459
                        <br><i class="fa fa-whatsapp"></i> 081548213835
                        <br><i class="fa fa-envelope"></i> butik.anis@gmail.com
                    <!-- /.col-md-3 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->


        <!-- *** COPYRIGHT ***
 _________________________________________________________ -->
        <div id="copyright">
            <div class="container">
                <div class="col-md-6">
                    <p class="pull-left">© 2016 Butik Anis</p>

                </div>
                <div class="col-md-6">
                    <p class="pull-right">Template by <a href="http://bootstrapious.com/e-commerce-templates">Bootstrapious</a> with support from <a href="https://kakusei.cz">Kakusei</a>
                        <!-- Not removing these links is part of the licence conditions of the template. Thanks for understanding :) -->
                    </p>
                </div>
            </div>
        </div>
        <!-- *** COPYRIGHT END *** -->



    </div>
    <!-- /#all -->
    <!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
    <script src="{{ asset('frontend/js/jquery-1.11.0.min.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.cookie.js') }}"></script>
    <script src="{{ asset('frontend/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('frontend/js/modernizr.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap-hover-dropdown.js') }}"></script>
    <script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('frontend/js/front.js') }}"></script>


</body>

</html>
